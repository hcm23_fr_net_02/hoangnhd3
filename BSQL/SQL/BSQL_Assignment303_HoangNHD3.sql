USE MASTER 
GO
DROP DATABASE IF EXISTS ESM2
GO 
CREATE DATABASE ESM2
GO
USE ESM2

--1
CREATE TABLE Employee (
	EmpNo int PRIMARY KEY,
	EmpName nvarchar(30),
	BirthDay date,
	Email varchar(30) UNIQUE,
	DeptNo int,
	MgrNo int DEFAULT 0 NOT NULL,
	StartDate date,
	Salary money,
	Level int CHECK (Level >= 1 AND Level <= 7) DEFAULT 0,
	Status int CHECK (Status IN (0, 1, 2)),
	Note nvarchar(MAX)
)

CREATE TABLE Skill (
	SkillNo int IDENTITY(1,1) PRIMARY KEY,
	SkillName varchar(30),
	Note varchar(MAX)
)

CREATE TABLE Emp_Skill (
	SkillNo int,
	EmpNo int,
	SkillLevel int CHECK (SkillLevel >= 1 AND SkillLevel <= 3),
	RegDate date,
	Description varchar(MAX),
	PRIMARY KEY (SkillNo, EmpNo),
	FOREIGN KEY (SkillNo) REFERENCES Skill(SkillNo),
	FOREIGN KEY (EmpNo) REFERENCES Employee(EmpNo)
)

CREATE TABLE Department (
	DeptNo int IDENTITY(1,1) PRIMARY KEY,
	DeptName varchar(30),
	Note varchar(MAX)
)

INSERT INTO Employee (EmpNo, EmpName, BirthDay, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note)
VALUES
	(1, 'Nguyen Van A', '1980-05-15', 'nguyenvana@gmail.com', 1, 12, '2018-09-13', 40000000, 7, 0, NULL),
	(2, 'Nguyen Van B', '1988-07-10', 'nguyenvanb@gmail.com', 2, 1, '2020-07-12', 28000000, 6, 1, NULL),
	(3, 'Nguyen Van C', '1983-12-04', 'nguyenvanc@gmail.com', 3, 1, '2019-08-18', 37000000, 7, 0, NULL),
	(4, 'Nguyen Van D', '1990-10-29', 'nguyenvand@gmail.com', 4, 13, '2023-07-10', 10000000, 1, 1, NULL),
	(5, 'Nguyen Van E', '1993-03-21', 'nguyenvane@gmail.com', 5, 2, '2022-12-20', 18000000, 3, 0, NULL),
	(6, 'Nguyen Van F', '1989-01-30', 'nguyenvanf@gmail.com', 6, 4, '2020-03-30', 26000000, 5, 0, NULL),
	(7, 'Nguyen Van G', '1997-08-28', 'nguyenvang@gmail.com', 7, 12, '2023-09-13', 5500000, 1, 1, NULL),
	(8, 'Nguyen Van H', '1991-11-09', 'nguyenvanh@gmail.com', 8, 8, '2020-10-24', 23000000, 4, 2, NULL),
	(9, 'Nguyen Van I', '1997-08-28', 'nguyenvani@gmail.com', 8, 8, '2023-01-04', 11000000, 2, 1, NULL),
	(10, 'Nguyen Van J', '1999-12-22', 'nguyenvanj@gmail.com', 8, 6, '2022-06-29', 17000000, 3, 2, NULL),
	(11, 'Nguyen Van K', '1987-04-14', 'nguyenvank@gmail.com', 3, 1, '2021-07-09', 24500000, 5, 0, NULL),
	(12, 'Nguyen Van L', '1981-02-12', 'nguyenvanl@gmail.com', 1, 5, '2019-02-28', 32000000, 6, 0, NULL),
	(13, 'Nguyen Van M', '1996-03-27', 'nguyenvanm@gmail.com', 1, 12, '2022-12-01', 22000000, 4, 0,NULL)

INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES 
	(1, 1, 3, '2019-07-29', NULL),
	(2, 3, 3, '2019-07-29', NULL),
	(3, 4, 3, '2019-07-29', NULL),
	(1, 6, 3, '2019-07-29', NULL),
	(1, 5, 3, '2019-07-29', NULL),
	(7, 9, 3, '2019-07-29', NULL),
	(1, 7, 3, '2019-07-29', NULL),
	(3, 11, 3, '2019-07-29', NULL),
	(6, 8, 3, '2019-07-29', NULL),
	(6, 13, 3, '2019-07-29', NULL),
	(2, 1, 3, '2019-07-29', NULL),
	(4, 1, 3, '2019-07-29', NULL),
	(1, 12, 3, '2019-07-29', NULL),
	(1, 13, 3, '2019-07-29', NULL),
	(3, 7, 3, '2019-07-29', NULL),
	(5, 2, 3, '2019-07-29', NULL),
	(8, 2, 3, '2019-07-29', NULL),
	(5, 11, 3, '2019-07-29', NULL),
	(4, 12, 3, '2019-07-29', NULL),
	(6, 2, 3, '2019-07-29', NULL),
	(7, 3, 3, '2019-07-29', NULL),
	(6, 12, 3, '2019-07-29', NULL),
	(5, 10, 3, '2023-05-15', NULL)


INSERT INTO Department (DeptName, Note) VALUES
	('AIC', 'Reasearch, Center for AI Research, Solution Consulting, and Business Intelligence'),
    ('GAM', 'Global Automotive Software Manufacturing, Business, and Development Unit'),
    ('DXG', 'Global Digital Transformation Unit'),
    ('SAS', 'Cybersecurity Service Development Unit'),
    ('CEP', 'Energy Sector Software Development Unit'),
	('EBS', 'Business Solutions and Services Unit'),
	('GHC', 'Global Healthcare Software Consulting and Development Unit'),
	('DES','UI Design and UX Consulting Unit')

INSERT INTO Skill (SkillName, Note)
VALUES
    ('C++', 'Used for system programming and embedded'),
    ('Java', 'OOP Language'),
    ('Python', 'OOP Language'),
    ('SQL', 'Use for manage relational database'),
    ('ReactJs', 'For front-end development'),
    ('Swift', 'Mobile development language'),
    ('.NET', 'Cross-platform frameworks for multiple fields'),
    ('NodeJS', 'Cross-platform JavaScript runtime environment')

--2
SELECT e.EmpName, e.Email, d.DeptName 
FROM Employee e
JOIN Department d
ON e.DeptNo = d.DeptNo
WHERE e.StartDate <= DATEADD(MONTH, -6, GETDATE())

--3
SELECT e.EmpName 
FROM Employee e
JOIN Emp_Skill es
ON es.EmpNo = e.EmpNo
JOIN Skill s
ON s.SkillNo = es.SkillNo
WHERE s.SkillName = 'C++' OR s.SkillName = '.NET'

--4
SELECT e1.EmpName AS EmployeeName, e2.EmpName AS ManagerName, e2.Email AS ManagerEmail 
FROM Employee e1
JOIN Employee e2
ON e1.MgrNo = e2.EmpNo

--5
SELECT d.DeptName as DevelopmentName, e.EmpName as EmployeeName
FROM Department d
JOIN Employee e
ON d.DeptNo = e.DeptNo
WHERE d.DeptNo IN (
	SELECT DeptNo
	FROM Employee
	GROUP BY DeptNo
	HAVING COUNT(*) >= 2
)
GROUP BY d.DeptName, e.EmpName

--6
SELECT e.EmpName AS EmployeeName, e.Email, COUNT(es.SkillNo) AS NumberOfSkills
FROM Employee e
JOIN Emp_Skill es
ON e.EmpNo = es.EmpNo
GROUP by e.EmpName, e.Email
ORDER BY e.EmpName ASC

--7
SELECT e.EmpName, e.Email, e.BirthDay
FROM Employee e
WHERE e.EmpNo IN (
		SELECT es.EmpNo
		FROM Emp_Skill es
		GROUP BY es.EmpNo
		HAVING COUNT(es.SkillNo) > 1
)
AND e.Status = 0 OR e.Status = 1

--8 
CREATE VIEW ListOfEmployees AS 
SELECT e.EmpName, s.SkillName, d.DeptName
FROM Employee e
JOIN Emp_Skill es
ON es.EmpNo = e.EmpNo
JOIN Skill s
ON s.SkillNo = es.SkillNo
JOIN Department d
ON d.DeptNo = e.DeptNo
WHERE e.Status = 0 OR e.Status = 1
GROUP BY e.EmpName, s.SkillName, d.DeptName