USE MASTER
GO
DROP DATABASE IF EXISTS FTM
GO
CREATE DATABASE FTM
GO
USE FTM
GO

--1
CREATE TABLE Trainee (
	TraineeID int IDENTITY(1,1) PRIMARY KEY,
	Full_Name varchar(30),
	Birth_Date date,
	Gender varchar(10),
	ET_IQ int CHECK (ET_IQ >= 0 AND ET_IQ <= 20),
	ET_Gmath int CHECK (ET_Gmath >= 0 AND ET_Gmath <= 20),
	ET_English int CHECK (ET_English >= 0 AND ET_English <= 50),
	Training_Class int,
	Evaluation_Note varchar(MAX)
)

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Note) VALUES 
	('John Doe', '1990-05-15', 'Male', 18, 15, 40, 101, 'Excellent performance in all subjects.'),
    ('Jane Smith', '1992-08-22', 'Female', 15, 10, 42, 102, 'Consistently good progress.'),
    ('Mike Johnson', '1995-02-10', 'Male', 8, 11, 38, 101, 'Showing improvement in English.'),
    ('Sarah Brown', '1993-11-30', 'Male', 3, 3, 15, 103, 'Bad performance in the class.'),
    ('David Lee', '1991-07-05', 'Female', 17, 16, 48, 102, 'Impressive IQ score.'),
    ('Emily Clark', '1994-04-20', 'Female', 5, 6, 36, 101, 'Needs improvement in mathematics.'),
    ('Daniel Wilson', '1996-09-12', 'Male', 16, 13, 44, 103, 'Strong performance overall.'),
    ('Olivia Turner', '1992-03-25', 'Female', 20, 20, 50, 102, 'Outstanding in all areas.'),
    ('William Davis', '1993-01-08', 'Male', 7, 7, 32, 101, 'Need working harder to catch up.'),
    ('Sophia Martinez', '1995-06-28', 'Female', 7, 9, 30, 102, 'Struggling with English.')

--2
TRUNCATE TABLE Trainee
ALTER TABLE Trainee ADD Fsoft_Account varchar(30) NULL;
UPDATE Trainee SET Fsoft_Account = CONCAT('Account_', TraineeID, '_', Full_Name);
ALTER TABLE Trainee	ALTER COLUMN Fsoft_Account varchar(30) NOT NULL;
ALTER TABLE Trainee ADD CONSTRAINT UQ_Fsoft_Account UNIQUE (Fsoft_Account);

--3
CREATE VIEW TraineeTest AS 
SELECT t.TraineeID, t.Full_Name, t.Gender, t.Birth_Date, t.ET_IQ, t.ET_Gmath, t.ET_English
FROM Trainee t
WHERE t.ET_IQ >= 8 AND t.ET_Gmath >= 8 AND t.ET_English >= 18 AND (t.ET_IQ + t.ET_Gmath) >= 20

--4
SELECT tt.Full_Name, MONTH(tt.Birth_Date)
FROM TraineeTest tt
GROUP BY tt.Full_Name, MONTH(tt.Birth_Date)
ORDER BY tt.Full_Name

--5
SELECT TOP 1 t.TraineeID, t.Full_Name, t.Gender, t.Birth_Date
FROM Trainee t
ORDER BY LEN(t.Full_Name) DESC