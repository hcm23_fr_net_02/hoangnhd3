USE MASTER
GO
DROP DATABASE IF EXISTS ESM
GO
CREATE DATABASE ESM
GO
USE ESM
GO

--Q1
CREATE TABLE Employee (
	EmpNo int PRIMARY KEY,
	EmpName nvarchar(30),
	BirthDate date,
	DeptNo int,
	MgrNo int,
	StartDate date,
	Salary decimal,
	Level int CHECK (Level >= 1 AND Level <= 7),
	Status int CHECK (Status IN (0, 1, 2)),
	Note nvarchar(MAX)
)

CREATE TABLE Skill (
	SkillNo int IDENTITY(1,1) PRIMARY KEY,
	SkillName varchar(30),
	Note varchar(MAX)
)

CREATE TABLE Emp_Skill (
	SkillNo int,
	EmpNo int,
	SkillLevel int CHECK (SkillLevel >= 1 AND SkillLevel <= 3),
	RegDate date,
	Description varchar(MAX),
	PRIMARY KEY (SkillNo, EmpNo),
	FOREIGN KEY (SkillNo) REFERENCES Skill(SkillNo),
	FOREIGN KEY (EmpNo) REFERENCES Employee(EmpNo)
)

CREATE TABLE Department (
	DeptNo int IDENTITY(1,1) PRIMARY KEY,
	DeptName varchar(30),
	Note varchar(MAX)
)

-- Q2
ALTER TABLE Employee ADD Email varchar(30) UNIQUE

ALTER TABLE Employee ADD CONSTRAINT df_MgrNo DEFAULT 0 FOR MgrNo
ALTER TABLE Employee ADD CONSTRAINT df_Status DEFAULT 0 FOR Status

--Q3
ALTER TABLE Employee ADD FOREIGN KEY (DeptNo) REFERENCES Department(DeptNo)
ALTER TABLE Emp_Skill DROP COLUMN Description

--Q4
INSERT INTO Employee (EmpNo, EmpName, BirthDate, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note, Email)
VALUES
    (1, 'John Doe', '1990-05-15', 1, 2, '2020-01-15', 55000.00, 3, 0, 'Good employee', 'johndoe@example.com'),
    (2, 'Jane Smith', '1985-08-22', 2, 1, '2018-03-20', 60000.00, 4, 0, 'Experienced manager', 'janesmith@example.com'),
    (3, 'Bob Johnson', '1995-02-10', 1, 2, '2021-07-10', 48000.00, 2, 1, 'On unpaid leave', 'bob@example.com'),
    (4, 'Alice Brown', '1992-11-30', 3, 1, '2019-11-05', 62000.00, 5, 0, 'Top performer', 'alice@example.com'),
    (5, 'David Lee', '1988-07-08', 1, 3, '2017-05-12', 58000.00, 4, 2, 'Out of office', 'david@example.com');


INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate)
VALUES
    (1, 1, 2, '2020-02-01'),
    (2, 1, 3, '2021-05-15'),
    (3, 2, 3, '2019-08-10'),
    (4, 3, 1, '2020-12-05'),
    (4, 4, 1, '2022-03-22'),
    (2, 4, 3, '2021-09-10'),
    (5, 5, 2, '2021-02-20'),
    (1, 3, 2, '2022-01-15'),
    (4, 5, 1, '2020-10-03');

INSERT INTO Skill (SkillName, Note)
VALUES
    ('Programming', 'Software development skills'),
    ('Database Management', 'Database administration skills'),
    ('Project Management', 'Project planning and management'),
    ('Communication', 'Effective communication skills'),
    ('Problem Solving', 'Analytical and problem-solving abilities');

INSERT INTO Department (DeptName, Note)
VALUES
    ('HR', 'Human Resources Department'),
    ('IT', 'Information Technology Department'),
    ('Finance', 'Finance Department'),
    ('Sales', 'Sales Department'),
    ('Marketing', 'Marketing Department');

CREATE VIEW EMPLOYEE_TRACKING AS 
SELECT EmpNo, EmpName AS Emp_Name, Level
FROM Employee
WHERE Level >= 3 AND Level <= 5;