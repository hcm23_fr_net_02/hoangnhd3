USE MASTER
GO
DROP DATABASE IF EXISTS DMS
GO
CREATE DATABASE DMS
GO
USE DMS
GO 

/****** Object:  Table [dbo].[EMPMAJOR]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPMAJOR](
	[emp_no] [char](6) NOT NULL,
	[major] [char](3) NOT NULL,
	[major_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[major] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMP]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMP](
	[emp_no] [char](6) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[dept_no] [char](3) NOT NULL,
	[job] [varchar](50) NULL,
	[salary] [money] NOT NULL,
	[bonus] [money] NULL,
	[ed_level] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DEPT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DEPT](
	[dept_no] [char](3) NOT NULL,
	[dept_name] [varchar](50) NOT NULL,
	[mgn_no] [char](6) NULL,
	[admr_dept] [char](3) NOT NULL,
	[location] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[dept_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[dept_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMPPROJACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPPROJACT](
	[emp_no] [char](6) NOT NULL,
	[proj_no] [char](6) NOT NULL,
	[act_no] [int] NOT NULL,
 CONSTRAINT [PK_EPA] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[proj_no] ASC,
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACT](
	[act_no] [int] NOT NULL,
	[act_des] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Dept]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[DEPT]  WITH CHECK ADD  CONSTRAINT [FK_Dept] FOREIGN KEY([mgn_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[DEPT] CHECK CONSTRAINT [FK_Dept]
GO
/****** Object:  ForeignKey [FK__EMP__dept_no__3E52440B]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMP]  WITH CHECK ADD FOREIGN KEY([dept_no])
REFERENCES [dbo].[DEPT] ([dept_no])
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_Major]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPMAJOR]  WITH CHECK ADD  CONSTRAINT [FK_Major] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPMAJOR] CHECK CONSTRAINT [FK_Major]
GO
/****** Object:  ForeignKey [FK_EPA1]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA1] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA1]
GO
/****** Object:  ForeignKey [FK_EPA2]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA2] FOREIGN KEY([act_no])
REFERENCES [dbo].[ACT] ([act_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA2]
GO

--1
INSERT INTO [dbo].[DEPT] ([dept_no], [dept_name], [mgn_no], [admr_dept], [location])
VALUES
    ('D01', 'HR', NULL, 'D01', 'New York'),
    ('D02', 'Engineering', NULL, 'D01', 'San Francisco'),
    ('D03', 'IT', NULL, 'D01', 'Los Angeles'),
    ('D04', 'Finance', NULL, 'D01', 'Chicago'),
    ('D05', 'Marketing', NULL, 'D01', 'Houston'),
    ('D06', 'Sales', NULL, 'D01', 'Miami'),
    ('D07', 'Operations', NULL, 'D01', 'Dallas'),
    ('D08', 'Legal', NULL, 'D01', 'Boston');

INSERT INTO [dbo].[EMP] ([emp_no], [last_name], [first_name], [dept_no], [job], [salary], [bonus], [ed_level])
VALUES
    ('E10001', 'Smith', 'John', 'D01', 'Manager', 60000.00, 5000.00, 4),
    ('E10002', 'Johnson', 'Alice', 'D02', 'Engineer', 55000.00, 4500.00, 3),
    ('E10003', 'Williams', 'David', 'D01', 'Analyst', 52000.00, 4000.00, 2),
    ('E10004', 'Jones', 'Sarah', 'D03', 'Technician', 48000.00, 3500.00, 2),
    ('E10005', 'Brown', 'Michael', 'D02', 'Engineer', 55000.00, 4500.00, 3),
    ('E10006', 'Davis', 'Jennifer', 'D03', 'Technician', 48000.00, 3500.00, 2),
    ('E10007', 'Miller', 'Robert', 'D01', 'Manager', 60000.00, 5000.00, 4),
    ('E10008', 'Wilson', 'Mary', 'D02', 'Engineer', 55000.00, 4500.00, 3);

INSERT INTO [dbo].[EMPMAJOR] ([emp_no], [major], [major_name])
VALUES
    ('E10001', 'M01', 'Computer Science'),
    ('E10002', 'M02', 'Electrical Engineering'),
    ('E10003', 'M03', 'Business Administration'),
    ('E10004', 'M04', 'Information Technology'),
    ('E10005', 'M02', 'Electrical Engineering'),
    ('E10006', 'M04', 'Information Technology'),
    ('E10007', 'M01', 'Computer Science'),
    ('E10008', 'M02', 'Electrical Engineering');

INSERT INTO [dbo].[EMPPROJACT] ([emp_no], [proj_no], [act_no])
VALUES
    ('E10001', 'P1001', 1),
    ('E10002', 'P1002', 2),
    ('E10003', 'P1001', 3),
    ('E10004', 'P1003', 1),
    ('E10005', 'P1002', 2),
    ('E10006', 'P1003', 3),
    ('E10007', 'P1001', 1),
    ('E10008', 'P1002', 2);

INSERT INTO [dbo].[ACT] ([act_no], [act_des])
VALUES
    (1, 'Design'),
    (2, 'Development'),
    (3, 'Testing'),
    (4, 'Documentation'),
    (5, 'Project Management'),
    (6, 'Quality Assurance'),
    (7, 'Research'),
    (8, 'Training');

--2
SELECT DISTINCT e.[emp_no], e.[last_name], e.[first_name]
FROM [dbo].[EMP] e
JOIN [dbo].[EMPPROJACT] epa ON e.[emp_no] = epa.[emp_no];

--3
SELECT e.[emp_no], e.[last_name], e.[first_name]
FROM [dbo].[EMP] e
JOIN [dbo].EMPMAJOR em
ON em.emp_no = e.emp_no
WHERE em.major_name = 'MAT' AND em.major_name = 'SCI'

--4
SELECT e.[emp_no], e.[first_name], e.[last_name]
FROM [dbo].[EMP] e
JOIN [dbo].[EMPPROJACT] epa
ON epa.emp_no = e.emp_no
JOIN [dbo].[ACT] A
ON a.act_no = epa.act_no
WHERE a.act_no BETWEEN 90 AND 110

--5
WITH DeptAggregates AS (
    SELECT
        [dept_no],
        COUNT(*) AS total_employees,
        AVG([salary]) AS avg_salary
    FROM [dbo].[EMP]
    GROUP BY [dept_no]
)

SELECT
    e.[emp_no],
    e.[last_name],
    e.[first_name],
    e.[salary],
    e.[dept_no],
    da.avg_salary AS dept_avg_sal
FROM [dbo].[EMP] e
JOIN DeptAggregates da ON e.[dept_no] = da.[dept_no]

--6
WITH DeptAvgEdLevel AS (
    SELECT
        e.[dept_no],
        AVG(e.[ed_level]) AS avg_ed_level
    FROM [dbo].[EMP] e
    GROUP BY e.[dept_no]
)

SELECT
	e.[dept_no],
    e.[emp_no],
    e.[last_name],
    e.[first_name],
    e.[ed_level]
FROM [dbo].[EMP] e
JOIN DeptAvgEdLevel dael ON e.[dept_no] = dael.[dept_no]
WHERE e.[ed_level] > dael.[avg_ed_level]
GROUP BY e.[dept_no], e.[emp_no], e.[last_name], e.[first_name], e.[ed_level];

--7
WITH TotalPayrolls AS (
	SELECT d.[dept_no], 
		   SUM(e.salary + e.bonus) AS total_payrolls
	FROM [dbo].[DEPT] d
	JOIN [dbo].[EMP] e
	ON e.[dept_no] = d.[dept_no]
	GROUP BY d.[dept_no]
)

SELECT TOP 1 d.dept_no, d.dept_name, tp.total_payrolls
FROM [dbo].[DEPT] d
JOIN TotalPayrolls tp
ON d.[dept_no] = tp.[dept_no]
GROUP BY d.[dept_no], d.[dept_name], tp.total_payrolls
ORDER BY tp.total_payrolls DESC

--8
SELECT TOP 5 e.[first_name], e.[last_name], e.[salary]
FROM [dbo].[EMP] e
GROUP BY e.[first_name], e.[last_name], e.[salary]
ORDER BY e.[salary] DESC