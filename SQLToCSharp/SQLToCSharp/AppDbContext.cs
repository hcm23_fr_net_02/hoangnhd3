﻿using Microsoft.EntityFrameworkCore;

namespace SQLToCSharp
{
    public class AppDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=.;Database=SqlToCSharp;Trusted_Connection=True;TrustServerCertificate=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                        .HasMany<Order>(c => c.Orders)
                        .WithOne(o => o.Customer)
                        .HasForeignKey(o => o.CustomerId);

            modelBuilder.Entity<OrderDetail>()
                        .HasOne<Order>(od => od.Order)
                        .WithMany(o => o.OrderDetails)
                        .HasForeignKey(od => od.OrderId);

            modelBuilder.Entity<OrderDetail>()
                        .HasOne<Product>(od => od.Product)
                        .WithMany(p => p.OrderDetails)
                        .HasForeignKey(od => od.ProductId);
        }
    }
}
