﻿/* STRING REVERSE */
//string a = "Hello World!";
//a.ReverseText();

//public static class Reverse
//{
//    public static void ReverseText(this string text)
//    {
//        char[] chars = text.ToCharArray();
//        string reverse = string.Concat(chars.Reverse());
//        Console.WriteLine(reverse);
//    }
//}

/* CALCULATE AVERAGE */
//List<int> numbers = new List<int>{ 2, 3, 7, 9 };

//Console.WriteLine(numbers.CalAvg());

//public static class Average
//{
//    public static double CalAvg(this List<int> numbers)
//    {
//        double avg = numbers.Average(); 
//        return avg;
//    }
//}

/* REMOVE DUPLICATES */
//using System.Text;

//string duplicate = "Hello Lord!";
//Console.WriteLine(duplicate.RemoveDuplicate());

//public static class Duplicate
//{
//    public static string RemoveDuplicate(this string duplicate)
//    {
//        StringBuilder sb = new StringBuilder();
//        HashSet<char> set = new HashSet<char>();

//        foreach (char s in duplicate)
//        {
//            if(!set.Contains(s))
//            {
//                sb.Append(s);
//                set.Add(s);
//            }
//        }
//        return sb.ToString();
//    }
//}