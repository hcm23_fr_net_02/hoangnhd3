﻿var count1 = CountDown(10);
var count2 = CountDown(5);
await count1;
await count2;

async Task CountDown(int seconds)
{
    for (int i = seconds; i >= 0; i--)
    {
        Console.WriteLine($"Remaining time: {i} seconds");
        await Task.Delay(1000); // chờ bất đồng bộ
    }
}