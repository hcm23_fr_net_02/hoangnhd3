﻿/* STACK */
//try
//{
//    Menu.StackMainMenu();
//}
//catch (Exception e)
//{
//    Console.WriteLine(e);
//}

//public class Menu
//{
//    static public void StackMainMenu()
//    {
//        Console.Write("Stack length: ");
//        int item = int.Parse(Console.ReadLine()!);
//        Stack stack = new Stack(item);
//        if (item < 0)
//        {
//            throw new Exception("Invalid stack length");
//        }
//        Console.WriteLine();

//        int choice = 0;
//        int choiceAmounts = 5;
//        while (choice != choiceAmounts)
//        {
//            Console.WriteLine("-------------");
//            Console.WriteLine($"{1}. Push");
//            Console.WriteLine($"{2}. Pop");
//            Console.WriteLine($"{3}. Peek");
//            Console.WriteLine($"{4}. Count");
//            Console.WriteLine($"{5}. Exit");
//            Console.WriteLine("-------------");
//            choice = int.Parse(Console.ReadLine()!);

//            if (choice > choiceAmounts)
//            {
//                Console.WriteLine("Invalid choice");
//            }
//            switch (choice)
//            {
//                case 1:
//                    Console.Write("Please input item you want to add: ");
//                    int item1 = int.Parse(Console.ReadLine()!);
//                    stack.Push(item1);
//                    break;
//                case 2:
//                    stack.Pop();
//                    break;
//                case 3:
//                    stack.Peek();
//                    break;
//                case 4:
//                    stack.Count();
//                    break;
//                default:
//                    break;
//            }
//        }
//    }
//}


//public class Stack
//{
//    int[] arr;
//    int top = -1;
//    int maxStack = 0;

//    public Stack(int _maxStack)
//    {
//        maxStack = _maxStack;
//        arr = new int[maxStack];
//    }

//    public void Push(int item)
//    {
//        if (top >= arr.Length - 1)
//        {
//            throw new Exception("Stack is full");
//        }
//        top++;
//        arr[top] = item;
//    }

//    public void Pop()
//    {
//        top--;
//        if (top <= -1)
//        {
//            throw new Exception("Stack is empty");
//        }
//    }

//    public void Peek()
//    {
//        Console.WriteLine();
//        Console.WriteLine($"Top item of stack is: {arr[top]}");
//    }

//    public void Count()
//    {
//        Console.WriteLine(arr.Count());
//    }
//}

/* QUEUE */
//try
//{
//    Menu.QueueMainMenu();
//}
//catch (Exception e)
//{
//    Console.WriteLine(e); ;
//}

//public class Menu
//{
//    static public void QueueMainMenu()
//    {
//        Console.Write("Queue length: ");
//        int item = int.Parse(Console.ReadLine()!);
//        Queue queue = new Queue(item);
//        if (item < 0)
//        {
//            throw new Exception("Invalid stack length");
//        }
//        Console.WriteLine();

//        int choice = 0;
//        int choiceAmounts = 5;
//        while (choice != choiceAmounts)
//        {
//            Console.WriteLine("-------------");
//            Console.WriteLine($"{1}. Enqueue");
//            Console.WriteLine($"{2}. Dequeue");
//            Console.WriteLine($"{3}. Peek");
//            Console.WriteLine($"{4}. Count");
//            Console.WriteLine($"{5}. Exit");
//            Console.WriteLine("-------------");
//            choice = int.Parse(Console.ReadLine()!);

//            if (choice > choiceAmounts)
//            {
//                Console.WriteLine("Invalid choice");
//            }
//            switch (choice)
//            {
//                case 1:
//                    Console.Write("Please input item you want to add: ");
//                    int item1 = int.Parse(Console.ReadLine()!);
//                    queue.Enqueue(item1);
//                    break;
//                case 2:
//                    queue.Dequeue();
//                    break;
//                case 3:
//                    queue.Peek();
//                    break;
//                case 4:
//                    queue.Count();
//                    break;
//                default:
//                    break;
//            }
//        }
//    }
//}

//public class Queue
//{
//    int[] arr;
//    int top = -1;
//    int bottom = -1;
//    int maxQueue;

//    public Queue(int _maxQueue)
//    {
//        maxQueue = _maxQueue;
//        arr = new int[maxQueue];
//        top = 0;
//    }

//    public void Enqueue(int item)
//    {
//        bottom++;
//        if (bottom == maxQueue)
//        {
//            throw new Exception("Queue is full");
//        }
//        arr[bottom] = item;
//    }

//    public void Dequeue()
//    {
//        if (top <= -1)
//        {
//            throw new Exception("Queue is empty");
//        }
//        if (top == bottom)
//        {
//            top = -1;
//            bottom = -1;
//        }
//        top++;
//    }

//    public void Peek()
//    {
//        Console.WriteLine();
//        Console.WriteLine(arr[top]);
//    }

//    public void Count()
//    {
//        Console.WriteLine(arr.Count());
//    }
//}
