﻿/* GENERIC */
//Pair<int> p1 = new Pair<int>(2, 3);
//Console.WriteLine($"{p1.First} and {p1.Second}");

//Pair<string> p2 = new Pair<string>("Di", "Hon");
//Console.WriteLine($"{p2.First} {p2.First}");

//Pair<char> p3 = new Pair<char>('d', 'h');
//Console.WriteLine($"{p3.First} {p3.Second}");

//Pair<double> p4 = new Pair<double>(2.5, 3.5);
//Console.WriteLine($"{p4.First} and {p4.Second}");

//class Pair<T>
//{
//    public T First { get; set; }
//    public T Second { get; set; }

//    public Pair(T _first, T _second)
//    {
//        First = _first;
//        Second = _second;
//    }
//}

/* GENERIC WITH MULTIPLE DATA */
//DataPair<int, string> dp1 = new DataPair<int, string>(1, "January");
//Console.WriteLine($"Thang {dp1.Data1} tieng anh la {dp1.Data2}");

//DataPair<double, bool> dp2 = new DataPair<double, bool>(2.5, false);
//Console.WriteLine($"Febuary la thang {dp2.Data1}, dung hay sai: {dp2.Data2}");

//class DataPair<T, U>
//{
//    public T Data1 { get; set; }
//    public U Data2 { get; set; }

//    public DataPair(T _data1, U _data2)
//    {
//        Data1 = _data1;
//        Data2 = _data2;
//}

/* GENERIC METHOD */
//string[] arr = { "Peter", "Thomas", "David" };
//int[] arr2 = {};
//A a = new A();
//a.GetLastItem(arr);
//a.GetLastItem(arr2);

//public class A
//{
//    public void GetLastItem<T>(T[] array)
//    {
//        int last = array.Length - 1;

//        try
//        {
//            if (array.Length == 0)
//            {
//                throw new Exception("Mang nay khong co phan tu");
//            }
//            Console.WriteLine(array[last]);
//        }
//        catch (Exception ex)
//        {
//            Console.WriteLine(ex);
//        }
//    }
//}

/* GENERIC STACK */
public class Menu
{
    static public void StackMainMenu()
    {
        Console.Write("Stack length: ");
        int item = int.Parse(Console.ReadLine()!);
        Stack<int> stack = new Stack<int>(item);
        if (item < 0)
        {
            throw new Exception("Invalid stack length");
        }
        Console.WriteLine();

        int choice = 0;
        int choiceAmounts = 5;
        while (choice != choiceAmounts)
        {
            Console.WriteLine("-------------");
            Console.WriteLine($"{1}. Push");
            Console.WriteLine($"{2}. Pop");
            Console.WriteLine($"{3}. Peek");
            Console.WriteLine($"{4}. Count");
            Console.WriteLine($"{5}. Exit");
            Console.WriteLine("-------------");
            choice = int.Parse(Console.ReadLine()!);

            if (choice > choiceAmounts)
            {
                Console.WriteLine("Invalid choice");
            }
            switch (choice)
            {
                case 1:
                    Console.Write("Please input item you want to add: ");
                    int item1 = int.Parse(Console.ReadLine()!);
                    stack.Push(item1);
                    break;
                case 2:
                    stack.Pop();
                    break;
                case 3:
                    stack.Peek();
                    break;
                case 4:
                    stack.Count();
                    break;
                default:
                    break;
            }
        }
    }
}

public class Stack<T>
{
    T[] arr;
    int top = -1;
    int maxStack;

    public Stack(int _maxStack)
    {
        maxStack = _maxStack;
        arr = new T[maxStack];
    }

    public void Push(T item)
    {
        if (top >= arr.Length - 1)
        {
            throw new Exception("Stack is full");
        }
        top++;
        arr[top] = item;
    }

    public void Pop()
    {
        top--;
        if (top <= -1)
        {
            throw new Exception("Stack is empty");
        }
    }

    public void Peek()
    {
        Console.WriteLine();
        Console.WriteLine($"Top item of stack is: {arr[top]}");
    }

    public void Count()
    {
        Console.WriteLine(arr.Count());
    }
}