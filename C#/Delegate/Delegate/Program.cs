﻿/* DELEGATE1 */
//using static Delegate1;

//MathOperation mathOperation1 = new MathOperation(Addition);
//MathOperation mathOperation2 = new MathOperation(Subtraction);
//MathOperation mathOperation3 = new MathOperation(Multiplication);
//MathOperation mathOperation4 = new MathOperation(Division);

//Delegate1.PerformOperation(6, 3, mathOperation1);
//Delegate1.PerformOperation(6, 3, mathOperation2);
//Delegate1.PerformOperation(6, 3, mathOperation3);
//Delegate1.PerformOperation(6, 3, mathOperation4);

//public class Delegate1
//{
//    public static void PerformOperation(int a, int b, MathOperation operation) => Console.WriteLine(operation(a, b))

//    public static int Addition(int a, int b) => a + b;
//    public static int Subtraction(int a, int b) => a - b;
//    public static int Multiplication(int a, int b) => a * b;
//    public static int Division(int a, int b) => a / b;

//    public delegate int MathOperation(int a, int b);
//}

/* DELEGATE2 */
//using static Delegate2;
//Custom custom1 = new Custom(Double);
//Custom custom2 = new Custom(Triple);
//Custom custom3 = new Custom(Square);
//Custom custom4 = new Custom(Cube);

//Delegate2.PrintResult(5, custom1);
//Delegate2.PrintResult(5, custom2);
//Delegate2.PrintResult(5, custom3);
//Delegate2.PrintResult(5, custom4);

//public class Delegate2
//{
//    public static void PrintResult(int number, Custom custom) => Console.WriteLine(custom(number));
//    public static int Double(int number) => number * 2;
//    public static int Triple(int number) => number * 3;
//    public static int Square(int number) => number * number;
//    public static int Cube(int number) => number * number * number;

//    public delegate int Custom(int number);
//}

/* ACTION */
//int[] numbers = { -1, 0, 2, -3 };
//Action action = new Action();
//action.PrintNumbers(numbers, action.action);

//public class Action
//{
//    public Action<int> action = MyAction;

//    public void PrintNumbers(int[] numbers, Action<int> action)
//    {
//        int[] result = new int[numbers.Length];
//        for(int i  = 0; i < numbers.Length; i++)
//        {
//            action(numbers[i]);
//        }
//    }

//    static void MyAction(int x)
//    {
//        if (x < 0)
//        {
//            Console.WriteLine("NEGATIVE");
//        }
//        else if (x > 0)
//        {
//            Console.WriteLine("POSITIVE");
//        }
//        else
//        {
//            Console.WriteLine("ZERO");
//        }
//    }
//}

//PrintNumbers(numbers, action);

/* FUNC */
//int[] numbers = { -1, 0, 2, -3, 4, 0, -5 };
//Function function = new();
//int[] results = function.ProcessNumbers(numbers, function.func);

//foreach (int result in results)
//{
//    Console.WriteLine(result);
//}

//public class Function
//{
//    public Func<int, int> func = MyFunc;
//    public int[] ProcessNumbers(int[] numbers, Func<int, int> func)
//    {
//        int[] result = new int[numbers.Length];
//        for (int i = 0; i < numbers.Length; i++)
//        {
//            result[i] = func(numbers[i]);
//        }
//        return result;
//    }

//    static int MyFunc(int x)
//    {
//        if (x > 0)
//        {
//            x = x * x;
//        }
//        else if (x < 0)
//        {
//            x = Math.Abs(x);
//        }
//        return x;
//    }
//}

/* PREDICATE */
//var words = new List<string> { "apple", "banana", "orange", "kiwi", "mango" };
//Predicate pre = new Predicate();

//List<string> filteredWords = pre.FilterWords(words, pre.pre);
//foreach (string word in filteredWords)
//{
//    Console.WriteLine(word);
//}

//public class Predicate
//{
//    public Predicate<string> pre = Filter;
//    public List<String> FilterWords(List<string> words, Predicate<string> pre)
//    {
//        var result = new List<string>();

//        foreach (var word in words)
//        {
//            if(pre(word))
//            {
//                result.Add(word);
//            }
//        };
//        return result;
//    }

//    static bool Filter(string word)
//    {
//        return word.Length >= 6;
//    }
//}