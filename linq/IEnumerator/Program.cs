﻿using System.Collections;
 
var arr = new int[] { 1, 2, 3, 4 };
var custom = new CustomEnumerator(arr);
 
while (custom.MoveNext())
{
    Console.WriteLine(custom.Current);
}
 
class CustomEnumerator : IEnumerator
{
    private int[] data;
    private int position = -1;
    public CustomEnumerator(int[] _data)
    {
        data = _data;
    }
    public object Current
    {
        get
        {
            if (position < 0 || position >= data.Length)
                throw new InvalidOperationException();
            return data[position] * 3;
        }
    }
 
    public bool MoveNext()
    {
        position++;
        return (position < data.Length);
    }
 
    public void Reset()
    {
        position = -1;
    }
}
