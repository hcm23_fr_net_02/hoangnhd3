﻿// Hãy giả định chúng ta có một danh sách các sản phẩm của một cửa hàng với các thuộc tính sau: 
// tên sản phẩm, giá, và danh mục (category). 
// Hãy tạo một danh sách các sản phẩm và thực hiện các yêu cầu sau bằng cách sử dụng Linq:

// Tạo danh sách các sản phẩm:
var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm dạ hội", Price = 1500000, Category = "Đầm" },
    // Thêm các sản phẩm khác tại đây...
};

// Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:
var products1 = products.Where(x => x.Price > 500000).Select(x => new { x.Name, x.Price, x.Category });

foreach (var product in products1)
{
    Console.WriteLine($"Name: {product.Name}, Price: {product.Price}, Category: {product.Category}");
}

// Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:
var products2 = products.Where(x => x.Price < 300000).Select(x => new { x.Name, x.Price, x.Category }).OrderBy(x => x.Price);

foreach (var product in products2)
{
    Console.WriteLine($"Name: {product.Name}, Price: {product.Price}, Category: {product.Category}");
}

// Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:
var products3 = products.GroupBy(x => x.Category);

foreach (var item in products3)
{
    Console.WriteLine($"Key: {item.Key}");
    foreach (var product in item)
    {
        Console.WriteLine(product.Name);
    }
}

// Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":
var productsSum = products.Sum(x => x.Price);

Console.WriteLine($"Tong gia tien cac san pham: {productsSum}");

// Tìm sản phẩm có giá cao nhất:
var maxPrice = products.Max(x => x.Price);

Console.WriteLine(maxPrice);

// Tạo danh sách các sản phẩm với tên đã viết hoa:
var products4 = products.Select(x => new { x.Name, x.Price, x.Category });

foreach (var product in products4)
{
    Console.WriteLine($"Name: {product.Name.ToUpper()}, Price: {product.Price}, Category: {product.Category}");
}

class Product
{
    public string Name { get; set; }
    public int Price { get; set; }
    public string Category { get; set; }
}