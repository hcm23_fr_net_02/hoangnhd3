﻿using System.Collections;
var custom = new CustomEnumerable();

foreach (var item in custom)
{
    Console.WriteLine(item);
}

class CustomEnumerable : IEnumerable
{
    public IEnumerator GetEnumerator()
    {
        var arr = new int[] { 1, 2, 3, 4 };
        var custom = new CustomEnumerator(arr);
        return custom;
    }
}

class CustomEnumerator : IEnumerator
{
    private int[] data;
    private int position = -1;
    public CustomEnumerator(int[] _data)
    {
        data = _data;
    }
    public object Current
    {
        get
        {
            if (position < 0 || position >= data.Length)
                throw new InvalidOperationException();
            return data[position] * 2;
        }
    }

    public bool MoveNext()
    {
        position++;
        return (position < data.Length);
    }

    public void Reset()
    {
        position = -1;
    }
}
