﻿// đọc file mock-data.json trong project để lấy dữ liệu

// sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới

//Lấy ra danh sách các FirstName của tất cả các nhân viên.

//Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.

//Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.

//Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".

//Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.

//Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.

//Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.

//Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.

//Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.

//Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.