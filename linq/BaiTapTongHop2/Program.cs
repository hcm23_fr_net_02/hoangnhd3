﻿// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlTypes;

var productsJson = File.ReadAllText("products.json");
var categoriesJson = File.ReadAllText("categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);


//Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.
/*var productsList1 = products.Where(x => x.Price > 100).Select(x => new { x.Name, x.Price }).OrderByDescending(x => x.Price);

foreach (var product in productsList1)
{
    Console.WriteLine($"Name: {product.Name}, Price: {product.Price}");
}*/

//Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
/*var productsCategories1 = products.Join(
                                categories,
                                products => products.CategoryId,
                                categories => categories.Id,
                                (product, categorie) => new
                                {
                                    CategoryName = categorie.Name,
                                }
                                ).OrderBy(x => x.CategoryName).GroupBy(x => x.CategoryName);

foreach (var item in productsCategories1)
{
    Console.WriteLine($"Key: {item.Key}");
    foreach (var category in item)
    {
        Console.WriteLine($"Category Name: {category.CategoryName}, Product Quantity: {item.Count()}");

    }
}*/

//Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
/*var productsCategories2 = products.Join(
                            categories,
                            products => products.CategoryId,
                            categories => categories.Id,
                            (products, categories) => new
                            {
                                CategoryName = categories.Name,
                                ProductPrice = products.Price
                            }
                            ).OrderBy(x => x.CategoryName).GroupBy(x => x.CategoryName);

foreach (var product in productsCategories2)
{
    Console.WriteLine($"Key: {product.Key}");
    var avgPrice = product.Average(x => x.ProductPrice);
    Console.WriteLine($"Average Price: {avgPrice}");
}*/

//Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.
/*var maxTenProducts = products.Select(x => new { x.Name, x.Price }).OrderByDescending(x => x.Price).Take(10);

foreach (var product in maxTenProducts)
{
    Console.WriteLine($"Name: {product.Name}, Price: {product.Price}");
}*/

//Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.
/*var productsCategories3 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        CategoryName = categories.Name,
                                        ProductPrice = products.Price
                                    }
                                    ).OrderBy(x => x.CategoryName).GroupBy(x => x.CategoryName);

foreach (var product in productsCategories3)
{
    Console.WriteLine($"Key: {product.Key}");
    var avgPrice = product.Average(x => x.ProductPrice);
    foreach (var item in product)
    {
        Console.WriteLine($"Category Name: {item.CategoryName}, Average Price: {avgPrice}");
    }
}*/

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.
/*var productsCategories4 = products.Where(x => x.Price < 50).Join(
                                categories,
                                products => products.CategoryId,
                                categories => categories.Id,
                                (products, categories) => new
                                {
                                    CategoryName = categories.Name,
                                    ProductName = products.Name
                                }
                                ).OrderBy(x => x.CategoryName);

foreach (var product in productsCategories4)
{
    Console.WriteLine(product.ProductName, product.CategoryName);
}*/

//Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
/*var productsCategories5 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        CategoryName = categories.Name,
                                        ProductPrice = products.Price
                                    }
                                    ).OrderBy(x => x.CategoryName).GroupBy(x => x.CategoryName);

foreach (var product in productsCategories5)
{
    Console.WriteLine($"{product.Key}");
    var totalPrice = product.Sum(x => x.ProductPrice);
    Console.WriteLine(totalPrice);
}*/

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần.
/*var productsCategories6 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        CategoryName = categories.Name,
                                        ProductName = products.Name
                                    }
                                    ).OrderBy(x => x.ProductName).GroupBy(x => x.CategoryName);

foreach (var product in productsCategories6)
{
    foreach (var item in product)
    {
        if (item.ProductName.Contains("Apple"))
        {
            Console.WriteLine($"Key: {product.Key}");
            Console.WriteLine(item.ProductName);
        }
    }
}*/

//Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.
/*var productsCategories7 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        CategoryName = categories.Name,
                                        ProductPrice = products.Price
                                    }
                                    ).OrderBy(x => x.CategoryName).GroupBy(x => x.CategoryName);

foreach (var product in productsCategories7)
{
    Console.WriteLine($"{product.Key}");
    var totalPrice = product.Sum(x => x.ProductPrice);
    if (totalPrice > 1000)
    {
        Console.WriteLine(totalPrice);
    }
}*/

//Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó. 
/*var productsCategories8 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        CategoryName = categories.Name,
                                        ProductPrice = products.Price
                                    }
                                    ).Where(x => x.ProductPrice < 10).Select(x => x.CategoryName);

foreach (var category in productsCategories8)
{
    Console.WriteLine(category);
}*/

//Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
/*var productsCategories9 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        CategoryName = categories.Name,
                                        ProductPrice = products.Price,
                                        ProductName = products.Name,
                                    }
                                    ).OrderBy(x => x.CategoryName).GroupBy(x => x.CategoryName);

foreach (var product in productsCategories9)
{
    Console.WriteLine($"Key: {product.Key}");
    var maxItem = product.Max(x => x.ProductPrice);
    foreach (var item in product)
    {
        if (item.ProductPrice == maxItem)
        {
            Console.WriteLine($"Name: {item.ProductName}, Price: {maxItem}");
            Console.WriteLine();
        }
    }
}*/

//Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.
var productsCategories10 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        CategoryName = categories.Name,
                                        ProductPrice = products.Price
                                    }
                                    ).GroupBy(x => x.CategoryName);

foreach (var product in productsCategories10)
{
    Console.WriteLine($"{product.Key}");
    var totalPrice = product.Sum(x => x.ProductPrice);
    var orderedProducts = product.OrderByDescending(x => totalPrice);   
}

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.
/*decimal average = products.Average(x => x.Price);
var productsCategories11 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        ProductName = products.Name,
                                        CategoryName = categories.Name,
                                        ProductPrice = products.Price
                                    }
                                    ).Where(x => x.ProductPrice > average);

Console.WriteLine(average);
foreach (var product in productsCategories11)
{
    Console.WriteLine($"Name: {product.ProductName}, Category: {product.ProductName}, Price: {product.ProductPrice}");
}*/

//Tính tổng số tiền của tất cả các sản phẩm.
/*var productTotal1 = products.Sum(x => x.Price);

Console.WriteLine(productTotal1);*/

//Lấy danh sách các danh mục và số lượng sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo số lượng sản phẩm giảm dần.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị giảm dần.

//Lấy danh sách các danh mục có ít sản phẩm nhất.
var productsCategories13 = products.Join(
                                    categories,
                                    products => products.CategoryId,
                                    categories => categories.Id,
                                    (products, categories) => new
                                    {
                                        CategoryName = categories.Name,
                                        ProductName = products.Name,
                                    }
                                    ).GroupBy(x => x.CategoryName).OrderBy(x => x.Count());

foreach (var category in productsCategories13)
{
    for (int i = 0; i < category.Count(); i++)
    {
        var minItem = category.Count();
        if (minItem < category.Count())
        {
            minItem = category.Count();
            Console.WriteLine(minItem);
        }
    }
}

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.

//Tính tổng số tiền của các sản phẩm có giá trị lớn hơn $50.
/*var productsTotal2 = products.Where(x => x.Price > 50).Sum(x => x.Price);

Console.WriteLine(productsTotal2);*/

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.


class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CategoryId { get; set; }
    public decimal Price { get; set; }
}

class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
}


