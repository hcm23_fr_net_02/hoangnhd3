USE MASTER
GO
DROP DATABASE IF EXISTS ESM
GO
CREATE DATABASE ESM
GO
USE ESM
GO

CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

GO
ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30) 
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

GO

--1
INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES
    (1, 'John Smith', '1980-05-15', 8, NULL, '2005-03-20', 60000.00, 1, NULL, 5, 'john.smith@example.com'),
    (2, 'Jane Doe', '1985-09-25', 4, 1, '2007-08-10', 55000.00, 1, NULL, 2, 'jane.doe@example.com'),
    (3, 'Michael Johnson', '1990-12-10', 7, 1, '2010-02-15', 70000.00, 2, NULL, 1, 'michael.j@example.com'),
    (4, 'Emily Davis', '1982-07-08', 2, 2, '2009-06-05', 62000.00, 1, NULL, 4, 'emily.d@example.com'),
    (5, 'David Wilson', '1993-03-18', 1, 2, '2011-04-12', 58000.00, 1, NULL, 3, 'david.w@example.com'),
    (6, 'Lisa Brown', '1989-10-29', 4, NULL, '2008-09-30', 65000.00, 2, NULL, 5, 'lisa.b@example.com'),
    (7, 'Robert Taylor', '1987-04-22', 4, 3, '2012-07-20', 72000.00, 1, NULL, 6, 'robert.t@example.com'),
    (8, 'Sarah Adams', '1995-06-14', 5, 3, '2014-01-03', 60000.00, 1, NULL, 7, 'sarah.a@example.com')	

INSERT INTO Department (DeptName, Note)
VALUES
    ('AIC', 'Reasearch, Center for AI Research, Solution Consulting, and Business Intelligence'),
    ('GAM', 'Global Automotive Software Manufacturing, Business, and Development Unit'),
    ('DXG', 'Global Digital Transformation Unit'),
    ('SAS', 'Cybersecurity Service Development Unit'),
    ('CEP', 'Energy Sector Software Development Unit'),
	('EBS', 'Business Solutions and Services Unit'),
	('GHC', 'Global Healthcare Software Consulting and Development Unit'),
	('DES','UI Design and UX Consulting Unit')


INSERT INTO Skill (SkillName, Note)
VALUES
    ('C++', 'Used for system programming and embedded'),
    ('Java', 'OOP Language'),
    ('Python', 'OOP Language'),
    ('SQL', 'Use for manage relational database'),
    ('ReactJs', 'For front-end development'),
    ('Swift', 'Mobile development language'),
    ('.NET', 'Cross-platform frameworks for multiple fields'),
    ('NodeJS', 'Cross-platform JavaScript runtime environment')

INSERT INTO Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES
    (1, 1, 5, '2020-02-01', 'Super good'),
    (2, 2, 4, '2019-11-15', 'Good'),
    (3, 3, 5, '2021-03-10', 'Good'),
    (4, 4, 4, '2018-08-20', 'Super good'),
    (5, 5, 3, '2019-04-05', 'Very good'),
    (6, 6, 5, '2020-06-12', 'Good'),
    (7, 7, 5, '2022-01-25', 'Extremely good'),
    (8, 8, 4, '2022-03-30', 'Super good');

--2
SELECT e.EmpName, e.Email, d.DeptName 
FROM Employee e
JOIN Department d
ON e.DeptNo = d.DeptNo
WHERE e.StartDate <= DATEADD(MONTH, -6, GETDATE())

--3
SELECT e.EmpName 
FROM Employee e
JOIN Emp_Skill es
ON es.EmpNo = e.EmpNo
JOIN Skill s
ON s.SkillNo = es.SkillNo
WHERE s.SkillName = 'C++' OR s.SkillName = '.NET'

--4
SELECT e1.EmpName AS EmployeeName, e2.EmpName AS ManagerName, e2.Email AS ManagerEmail 
FROM Employee e1
JOIN Employee e2
ON e1.MgrNo = e2.EmpNo

--5
SELECT d.DeptName as DevelopmentName, e.EmpName as EmployeeName
FROM Department d
JOIN Employee e
ON d.DeptNo = e.DeptNo
WHERE d.DeptNo IN (
	SELECT DeptNo
	FROM Employee
	GROUP BY DeptNo
	HAVING COUNT(*) >= 2
)
GROUP BY d.DeptName, e.EmpName

--6
SELECT e.EmpName AS EmployeeName, e.Email, COUNT(es.SkillNo) AS NumberOfSkills
FROM Employee e
JOIN Emp_Skill es
ON e.EmpNo = es.EmpNo
GROUP by e.EmpName, e.Email
ORDER BY e.EmpName DESC

--7
SELECT e.EmpName, e.Email, e.BirthDay
FROM Employee e
WHERE e.EmpNo IN (
		SELECT es.EmpNo
		FROM Emp_Skill es
		GROUP BY es.EmpNo
		HAVING COUNT(es.SkillNo) > 1
)
AND e.Status = 0 OR e.Status = 1

--8
CREATE VIEW ListOfEmployees AS 
SELECT e.EmpName, s.SkillName, d.DeptName
FROM Employee e
JOIN Emp_Skill es
ON es.EmpNo = e.EmpNo
JOIN Skill s
ON s.SkillNo = es.SkillNo
JOIN Department d
ON d.DeptNo = e.DeptNo
WHERE e.Status = 0 OR e.Status = 1