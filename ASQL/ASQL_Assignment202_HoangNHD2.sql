USE MASTER
GO
DROP DATABASE IF EXISTS EMS
GO
CREATE DATABASE EMS
GO
USE EMS
GO

CREATE TABLE [dbo].[Employee](
        [EmpNo] [int] NOT NULL,
        [EmpName] [nchar](30) COLLATE
        SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [BirthDay] [datetime] NOT NULL,
        [DeptNo] [int] NOT NULL,
        [MgrNo] [int] NOT NULL,
        [StartDate] [datetime] NOT NULL,
        [Salary] [money] NOT NULL,
        [Status] [int] NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL,
        [Level] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE Employee
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  ADD  CONSTRAINT [chk_Level] CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO

ALTER TABLE [dbo].[Employee]  ADD  CONSTRAINT [chk_Status] CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))
GO

ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30)
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] ADD CONSTRAINT chk_Email1 UNIQUE(Email)
GO

ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo
GO

ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status
GO

CREATE TABLE [dbo].[Skill](
        [SkillNo] [int] IDENTITY(1,1) NOT NULL,
        [SkillName] [nchar](30) COLLATE
         SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)
GO

CREATE TABLE [dbo].[Department](
        [DeptNo] [int] IDENTITY(1,1) NOT NULL,
        [DeptName] [nchar](30) COLLATE
        SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)
GO

CREATE TABLE [dbo].[Emp_Skill](
        [SkillNo] [int] NOT NULL,
        [EmpNo] [int] NOT NULL,
        [SkillLevel] [int] NOT NULL,
        [RegDate] [datetime] NOT NULL,
        [Description] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)


--2
INSERT INTO Department (DeptName, Note)
VALUES ('HR', 'Human Resources Department'),
       ('IT', 'Information Technology Department'),
       ('Sales', 'Sales Department'),
       ('Marketing', 'Marketing Department'),
       ('Finance', 'Finance Department'),
       ('Engineering', 'Engineering Department'),
       ('Operations', 'Operations Department'),
       ('Legal', 'Legal Department');

INSERT INTO Skill (SkillName, Note)
VALUES ('Java', 'Java programming language'),
       ('SQL', 'Structured Query Language'),
       ('JavaScript', 'JavaScript programming language'),
       ('Python', 'Python programming language'),
       ('Project Management', 'Project management skills'),
       ('Data Analysis', 'Data analysis skills'),
       ('Communication', 'Communication skills'),
       ('Problem Solving', 'Problem-solving skills');

INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES (1, 'John Doe', '1990-05-15', 1, 0, '2015-03-20', 60000, 1, 'HR Manager', 6, 'john.doe@example.com'),
       (2, 'Jane Smith', '1985-09-25', 2, 1, '2018-02-10', 70000, 0, 'IT Specialist', 5, 'jane.smith@example.com'),
       (3, 'Michael Johnson', '1988-12-10', 3, 1, '2017-07-05', 65000, 1, 'Sales Associate', 4, 'michael.johnson@example.com'),
       (4, 'Emily Wilson', '1992-03-02', 4, 2, '2019-01-15', 55000, 1, 'Marketing Coordinator', 1, 'emily.wilson@example.com'),
       (5, 'David Brown', '1987-06-18', 1, 0, '2016-05-08', 62000, 1, 'HR Specialist', 5, 'david.brown@example.com'),
       (6, 'Sarah Lee', '1993-08-20', 5, 3, '2020-04-22', 72000, 1, 'Finance Analyst', 1, 'sarah.lee@example.com'),
       (7, 'Alex Turner', '1991-11-28', 6, 4, '2018-09-12', 68000, 0, 'Software Engineer', 3, 'alex.turner@example.com'),
       (8, 'Olivia Clark', '1994-02-14', 7, 5, '2022-11-30', 66000, 1, 'Operations Coordinator', 1, 'olivia.clark@example.com');

INSERT INTO Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES (1, 1, 5, '2016-01-10', 'Java certification'),
       (2, 2, 4, '2018-03-25', 'SQL proficiency'),
       (3, 3, 3, '2017-09-14', 'JavaScript front-end development'),
       (4, 4, 4, '2019-02-02', 'Python data analysis'),
       (5, 5, 4, '2016-07-12', 'Project management certification'),
       (6, 6, 5, '2020-05-30', 'Data analysis skills'),
       (7, 7, 5, '2018-10-18', 'Excellent communication skills'),
       (8, 8, 3, '2019-12-05', 'Problem-solving abilities');

--3
CREATE PROCEDURE UpdateLevel AS 
UPDATE Employee 
SET Level = 2 
WHERE YEAR(GETDATE()) - YEAR(StartDate) >= 3 AND Level = 1

DECLARE @UpdatedCount INT;
SET @UpdatedCount = @@ROWCOUNT;

PRINT 'Number of updated records: ' + CAST(@UpdatedCount AS NVARCHAR(10));

EXEC UpdateLevel

--4
CREATE PROCEDURE EmployeeOut @EmpNo int AS
SET NOCOUNT ON

DECLARE @EmpName nchar(30);
DECLARE @EmpEmail nchar(30);
DECLARE @DeptName nchar(30);

SELECT @EmpEmail = e.EmpName, @EmpEmail = e.Email, @DeptName = d.DeptName
FROM Employee e
JOIN Department d
ON d.DeptNo = e.DeptNo
WHERE e.EmpNo = @EmpNo

IF @@ROWCOUNT > 0
BEGIN 
	PRINT 'Employe name: ' + @EmpName
	PRINT 'Employee email: ' + @EmpEmail
	PRINT	'Department name: ' + @DeptName
END
ELSE 
BEGIN 
	PRINT 'Employee with employee number ' + CAST(@EmpNo AS nvarchar(10)) + ' not found'
END

EXEC EmployeeOut 2

--5
CREATE FUNCTION Emp_Tracking (@EmpNo int) 
RETURNS MONEY
AS 
BEGIN 
	DECLARE @Salary MONEY;

	SELECT @Salary = Salary
	FROM Employee
	WHERE EmpNo = @EmpNo

	RETURN @Salary
END

SELECT dbo.Emp_Tracking(1)


--6
CREATE TRIGGER PreventInvalidEmployee
ON Employee
AFTER INSERT, UPDATE
AS
BEGIN
    SET NOCOUNT ON;

    IF EXISTS (
        SELECT 1
        FROM INSERTED
        WHERE Level = 1 AND Salary > 10000000
    )
    BEGIN
        ROLLBACK TRANSACTION;
    END
END;

INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
	VALUES (10, 'Hon Di', '1990-05-15', 1, 0, '2015-03-20', 11000000, 1, 'HR Manager', 1, 'hon.di@example.com')