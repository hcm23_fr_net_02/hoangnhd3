-- QUESTION 1
USE MASTER 
GO
DROP DATABASE IF EXISTS OrderManagement
GO 
CREATE DATABASE OrderManagement
GO
USE OrderManagement

--1
CREATE TABLE San_Pham (
	Ma_SP int PRIMARY KEY IDENTITY(1,1),
	Ten_SP varchar(30),
	Don_Gia decimal(6, 2)
)

CREATE TABLE Khach_Hang (
	Ma_KH int PRIMARY KEY IDENTITY(1,1),
	Ten_KH varchar(30),
	Phone_No varchar(30),
	Ghi_Chu varchar(MAX)
)

CREATE TABLE Don_Hang (
	Ma_DH int PRIMARY KEY IDENTITY(1,1),
	Ngay_DH date,
	MA_SP int,
	So_Luong int,
	Ma_KH int,
	FOREIGN KEY(Ma_SP) REFERENCES San_Pham(Ma_SP),
	FOREIGN KEY(Ma_KH) REFERENCES Khach_Hang(Ma_KH)
)

INSERT INTO San_Pham VALUES 
	('Macbook Pro 14', 2199.99),
	('Dell Alienware M15 R6', 2499.99),
	('Asus ROG Zepherus G16', 1899.99)

INSERT INTO Khach_Hang VALUES
	('Nguyen Quoc Khach', '0909009009', 'Khach hang VIP'),
	('Truong Anh Kiet', '0911121314', 'Khach hang VIP'),
	('Nguyen Huu Duy Hoang', '0399123456', 'Khach hang VIP')

INSERT INTO Don_Hang VALUES 
	('2022-09-23', 3, 10, 1),
	('2022-05-15', 1, 5, 2),
	('2022-02-28', 2, 7, 3)

--2
CREATE VIEW OrderSlip AS
SELECT kh.Ten_KH, dh.Ngay_DH, sp.Ten_SP, dh.So_Luong, (sp.Don_Gia * dh.So_Luong) AS Thanh_Tien
FROM Khach_Hang kh
JOIN Don_Hang dh
ON dh.Ma_KH = kh.Ma_KH
JOIN San_Pham sp
ON sp.Ma_SP = dh.MA_SP

--QUESTION 2
USE MASTER
GO
DROP DATABASE IF EXISTS ESM
GO
CREATE DATABASE ESM
GO
USE ESM
GO

--1
CREATE TABLE Employee_Table (
	Employee_Number int PRIMARY KEY IDENTITY(1,1),
	Employee_Name varchar(30),
	Department_Number int,
	FOREIGN KEY (Department_Number) REFERENCES Department(Department_Number)
)

CREATE TABLE Department (
	Department_Number int PRIMARY KEY IDENTITY(1,1),
	Department_Name varchar(30)
)

CREATE TABLE Skill_Table (
	Skill_Code int PRIMARY KEY IDENTITY(1,1),
	Skill_Name varchar(30)
)

CREATE TABLE Employee_Skill_Table (
	Employee_Number int,
	Skill_Code int,
	Date_Registered date,
	PRIMARY KEY (Employee_Number, Skill_Code),
	FOREIGN KEY (Employee_Number) REFERENCES Employee_Table(Employee_Number),
	FOREIGN KEY (Skill_Code) REFERENCES Skill_Table(Skill_Code)
)

INSERT INTO Department VALUES 
	('FHN'),
	('FHM'),
	('FDN')

INSERT INTO Skill_Table VALUES 
	('Java'),
	('.NET'),
	('Python')

INSERT INTO Employee_Table VALUES 
	('Nguyen Van A', 1),
	('Nguyen Van B', 2),
	('Nguyen Van C', 3),
	('Nguyen Van D', 3),
	('Nguyen Van E', 2),
	('Nguyen Van F', 1),
	('Nguyen Van G', 2),
	('Nguyen Van H', 1),
	('Nguyen Van I', 3),
	('Nguyen Van J', 3),
	('Nguyen Van K', 1),
	('Nguyen Van L', 2),
	('Nguyen Van M', 2),
	('Nguyen Van N', 1)

INSERT INTO Employee_Skill_Table VALUES
	(1, 2, '2023-09-01'),	
	(2, 3, '2022-07-18'),
	(3, 1, '2020-11-21'),
	(4, 3, '2023-02-28'),
	(5, 1, '2018-01-31'),
	(6, 2, '2019-10-11'),
	(7, 1, '2020-04-23'),
	(8, 2, '2022-09-04'),
	(9, 3, '2020-08-02'),
	(10, 2, '2018-01-24'),
	(11, 1, '2022-10-30'),
	(12, 1, '2023-05-05'),
	(13, 3, '2021-12-06'),
	(14, 1, '2021-03-15')

--2
SELECT et.Employee_Name, st.Skill_Name
FROM Employee_Table et
JOIN Employee_Skill_Table est
ON est.Employee_Number = et.Employee_Number
JOIN Skill_Table st
ON st.Skill_Code = est.Skill_Code
WHERE Skill_Name = 'Java'

--3
SELECT d.Department_Name, et.Employee_Name
FROM Department d
JOIN Employee_Table et
ON et.Department_Number = d.Department_Number
WHERE et.Department_Number IN (
	SELECT et.Department_Number
	FROM Employee_Table et
	GROUP BY et.Department_Number
	HAVING COUNT(*) >= 3
)
GROUP BY d.Department_Name, et.Employee_Name

--4
SELECT et.Employee_Number, et.Employee_Name, st.Skill_Name
FROM Employee_Table et
JOIN Employee_Skill_Table est
ON est.Employee_Number = et.Employee_Number
JOIN Skill_Table st
ON st.Skill_Code = est.Skill_Code
WHERE est.Employee_Number IN (
	SELECT est.Employee_Number
	FROM Employee_Skill_Table est
	GROUP BY est.Employee_Number
	HAVING COUNT(est.Employee_Number) > 1
)

--Add more skill to employee
INSERT INTO Employee_Skill_Table VALUES 
	(2, 2, '2021-12-29'),
	(2, 1, '2021-08-12'),
	(1, 1, '2023-03-17')

--5
CREATE VIEW MultipleSkillsEmployees AS
SELECT et.Employee_Number, et.Employee_Name, d.Department_Name
FROM Employee_Table et
JOIN Employee_Skill_Table est
ON est.Employee_Number = et.Employee_Number
JOIN Skill_Table st
ON st.Skill_Code = est.Skill_Code
JOIN Department d 
ON d.Department_Number = et.Department_Number
WHERE est.Employee_Number IN (
	SELECT est.Employee_Number
	FROM Employee_Skill_Table est
	GROUP BY est.Employee_Number
	HAVING COUNT(est.Employee_Number) > 1
)
