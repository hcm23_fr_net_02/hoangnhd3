USE MASTER 
GO
DROP DATABASE IF EXISTS ABC
GO
CREATE DATABASE ABC
GO
USE ABC
GO

-- 2a
CREATE TABLE Employee (
	EmployeeID int IDENTITY(1,1) PRIMARY KEY,
	EmployeeLastName varchar(20),
	EmployeeFirstName varchar(20),
	EmployeeHireDate date,
	EmployeeStatus bit,
	SupervisorID int,
	SocialSecurityNumber varchar(10)
)

CREATE TABLE Projects (
	ProjectID int IDENTITY(1,1) PRIMARY KEY,
	ProjectManagerID int,
	ProjectName varchar(30),
	ProjectStartDate date,
	ProjectDescription varchar(MAX),
	ProjectDetail varchar(MAX),
	ProjectCompletedOn date,
	FOREIGN KEY (ProjectManagerID) REFERENCES Employee(EmployeeID)
)

CREATE TABLE Project_Modules (
	ModuleID int IDENTITY(1,1) PRIMARY KEY,
	ProjectID int,
	EmployeeID int,
	ProjectModulesDate date,
	ProjectModulesCompletedOn date,
	ProjectModulesDescription varchar(MAX)
	FOREIGN KEY (ProjectID) REFERENCES Projects(ProjectID),
	FOREIGN KEY (EmployeeID) REFERENCES Employee(EmployeeID),
)
CREATE TABLE Work_Done (
	WorkDoneID int IDENTITY(1,1) PRIMARY KEY,
	ModuleID int,
	EmployeeID int,
	WorkDoneDate date,
	WorkDoneDescription varchar(30),
	WorkDoneStatus bit
	FOREIGN KEY (ModuleID) REFERENCES Project_Modules(ModuleID),
	FOREIGN KEY (EmployeeID) REFERENCES Employee(EmployeeID)
)

insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Rimell', 'Tiffi', '10/11/2020', 0, 8, 9820);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Hischke', 'Edna', '11/20/2021', 0, 2, 5428);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Purchall', 'Ava', '09/13/2023', 0, 8, 6955);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Callaway', 'Cheslie', '07/06/2019', 0, 8, 5336);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Robinett', 'Nolan', '11/18/2020', 0, 10, 4060);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Phifer', 'Annamaria', '01/31/2021', 0, 3, 1407);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Crook', 'Logan', '03/09/2021', 1, 7, 9765);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Geffe', 'Chrissy', '11/29/2021', 0, 5, 6648);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Kelley', 'Honoria', '11/20/2018', 0, 4, 2689);
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber) values ('Benian', 'Lexi', '10/17/2020', 1, 5, 2556);

insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (8, 'Capreolus capreolus', '10/14/2018', 'faucibus', 'libero', '8/26/2023');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (3, 'Trichoglossus chlorolepidotus', '05/19/2021', 'in', 'ut', '7/29/2023');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (3, 'Halcyon smyrnesis', '03/14/2021', 'nibh', 'aliquam', '11/2/2022');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (4, 'Marmota flaviventris', '05/23/2020', 'hac', 'feugiat', '6/27/2023');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (6, 'Kobus defassa', '10/01/2022', 'vel', 'blandit', '12/1/2022');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (1, 'Castor canadensis', '03/18/2021', 'mauris', 'eros', '8/22/2023');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (9, 'Procyon lotor', '01/08/2023', 'vestibulum', 'leo', '5/9/2023');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (7, 'Dusicyon thous', '07/29/2021', 'nulla', 'vestibulum', '9/21/2023');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (7, 'Corvus albicollis', '02/15/2022', 'cras', 'in', '10/22/2022');
insert into Projects (ProjectManagerID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn) values (10, 'Corvus brachyrhynchos', '05/18/2023', 'tincidunt', 'nulla', '12/5/2022');

insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (2, 9, '08/23/2023', '12/5/2022', 'sit');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (7, 6, '09/16/2021', '11/25/2022', 'vel');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (8, 3, '07/29/2021', '7/13/2023', 'luctus');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (2, 2, '10/13/2021', '7/18/2023', 'tristique');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (7, 3, '12/07/2018', '6/28/2023', 'sagittis');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (9, 5, '12/09/2020', '9/16/2023', 'turpis');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (1, 8, '04/23/2021', '8/30/2023', 'sapien');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (1, 8, '06/23/2019', '8/14/2023', 'vehicula');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (6, 6, '01/01/2019', '6/9/2023', 'lobortis');
insert into Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription) values (4, 1, '07/05/2021', '7/6/2023', 'pede');

insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (2, 4, '03/29/2021', '10/6/2022', 0);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (5, 6, '11/23/2022', '9/9/2023', 0);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (3, 5, '07/25/2022', '11/10/2022', 1);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (9, 3, '07/12/2019', '3/6/2023', 0);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (5, 2, '12/24/2019', '2/2/2023', 0);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (1, 7, '08/19/2021', '7/10/2023', 1);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (6, 8, '12/23/2022', '7/7/2023', 1);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (1, 4, '05/17/2021', '2/20/2023', 1);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (1, 1, '09/17/2022', '9/7/2023', 0);
insert into Work_Done (ModuleID, EmployeeID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus) values (6, 4, '02/11/2019', '6/16/2023', 1);

--2b
CREATE PROCEDURE ActiveModule
AS (
	SELECT pm.ModuleID, e.EmployeeID, e.EmployeeFirstName, e.EmployeeLastName
	FROM Project_Modules pm
	JOIN Employee e
	ON e.EmployeeID = pm.EmployeeID
)

EXEC ActiveModule

--2c
CREATE FUNCTION WorkInformation(@EmployeeID int) 
RETURNS TABLE
AS 
RETURN (
	SELECT WD.WorkDoneID, M.ProjectID, P.ProjectName, M.ProjectModulesDescription, WD.WorkDoneDate, WD.WorkDoneDescription, WD.WorkDoneStatus
    FROM Work_Done WD
    JOIN Project_Modules M 
	ON WD.ModuleID = M.ModuleID
    JOIN Projects P 
	ON M.ProjectID = P.ProjectID
    WHERE WD.EmployeeID = @EmployeeID
    UNION
    SELECT WD.WorkDoneID, M.ProjectID, P.ProjectName, M.ProjectModulesDescription, WD.WorkDoneDate, WD.WorkDoneDescription, WD.WorkDoneStatus
    FROM Work_Done WD
    JOIN Project_Modules M 
	ON WD.ModuleID = M.ModuleID
    JOIN Projects P 
	ON M.ProjectID = P.ProjectID
    WHERE M.EmployeeID = @EmployeeID
) 

SELECT * FROM dbo.WorkInformation(1)
SELECT * FROM Employee

--2d
CREATE TRIGGER CheckProjectStartDate
ON Projects
AFTER INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM INSERTED
        WHERE ProjectStartDate > GETDATE()
    )
    BEGIN
        ROLLBACK TRANSACTION;
    END
END

CREATE TRIGGER CheckProjectCompletionDate
ON Projects
AFTER INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM INSERTED
        WHERE ProjectCompletedOn < ProjectStartDate
    )
    BEGIN
        ROLLBACK TRANSACTION;
    END
END

CREATE TRIGGER CheckProjectModulesDate
ON Project_Modules
AFTER INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM INSERTED I
        JOIN Projects P 
		ON I.ProjectID = P.ProjectID
        WHERE I.ProjectModulesDate < P.ProjectStartDate OR I.ProjectModulesDate > P.ProjectCompletedOn
    )
    BEGIN
        ROLLBACK;
    END
END