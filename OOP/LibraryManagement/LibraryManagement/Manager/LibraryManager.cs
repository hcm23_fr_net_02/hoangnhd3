﻿using LibraryManagement.Models;

namespace LibraryManagement.Manager
{
    public abstract class AbLibraryManager
    {
        public abstract Book AddBook(Book book);
        public abstract Book DeleteBook(int id);
        public abstract List<Book> ShowBookList(int pageNumber, int pageSize);
        public abstract Book GetBookById(int id);
    }

    public class LibraryManager : AbLibraryManager
    {
        Library library = new Library();
        public override Book AddBook(Book book)
        {
            library.Books.Add(book);
            return book;
        }

        public override Book DeleteBook(int id)
        {
            var deletedBook = GetBookById(id);
            library.Books.Remove(deletedBook);
            return deletedBook;
        }

        public override List<Book> ShowBookList(int pageNumber, int pageSize)
        {
            var bookList = library.Books.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            foreach (var book in library.Books)
            {
                Console.WriteLine($"Id: {book.Id}, Title: {book.Title}, Author: {book.Author}, Price: {book.Price}, Quantity: {book.StockQuantity}");
            }

            return bookList;
        }

        public override Book GetBookById(int id)
        {
            var idBook = library.Books.FirstOrDefault(x => x.Id == id);
            return idBook!;
        }
    }
}
