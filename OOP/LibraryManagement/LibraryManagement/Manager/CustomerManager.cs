﻿using LibraryManagement.Models;

namespace LibraryManagement.Manager
{
    public abstract class AbCustomerManager
    {
        public abstract void BorrowBook(string customerName, string title, int quantity);
    }

    public class CustomerManager : AbCustomerManager
    {
        private readonly Loan loan = new Loan();
        private readonly Library library = new Library();
        int nextId = 1;

        public override void BorrowBook(string customerName, string title, int quantity)
        {
            var borrowedCustomer = Customer.customers.FirstOrDefault(x => x.FullName == customerName);

            if (borrowedCustomer is null)
            {
                Console.WriteLine("Customer not found!");
            }

            var borrowedBook = library.Books!.FirstOrDefault(x => x.Title == title);
            
            if (quantity > borrowedBook!.StockQuantity)
            {
                Console.WriteLine("Cannot borrow more books!");
            }

            if (quantity <= 0)
            {
                Console.WriteLine("Invalid quantity!");
            }

            else
            {
                loan.Id = nextId++;
                loan.LoanDate = DateTime.Now;
                loan.DueDate = DateTime.Now.AddMonths(3);
                loan.Customer = borrowedCustomer;

                for (int i = 0; i < quantity; i++)
                {
                    loan.Books!.Add(borrowedBook!);
                }

                borrowedBook!.StockQuantity -= quantity;

                borrowedCustomer!.Loans.Add(loan);
            }
        }
    }
}
