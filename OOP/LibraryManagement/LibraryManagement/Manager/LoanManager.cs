﻿using LibraryManagement.Models;

namespace LibraryManagement.Manager
{
    public abstract class AbLoanManager
    {
        public abstract void GetLoanBooks(string name);
    }

    public class LoanManager : AbLoanManager
    {
        Loan loan = new Loan();

        public override void GetLoanBooks(string name)
        {
            var borrowedCustomer = Customer.customers.FirstOrDefault(x => x.FullName == name);

            foreach (var loan in borrowedCustomer!.Loans) 
            {
                Console.WriteLine($"Loan Date: {loan.LoanDate}, Due Date: {loan.DueDate}, Borrowed Books: ");

                foreach (var book in loan.Books!)
                {
                    Console.WriteLine($"Id: {book.Id}, Title: {book.Title}, Author: {book.Author}, Price: {book.Price}");
                }
            }
        }
    }
}
