﻿using LibraryManagement.Models;

namespace LibraryManagement.Manager
{
    public class MenuManager
    {
        AbLibraryManager abLibraryManager = new LibraryManager();
        Library library = new Library();

        public MenuManager(AbLibraryManager _abLibraryManager)
        {
            abLibraryManager = _abLibraryManager;
        }

        public void MainMenu()
        {
            int choice = 0;
            int choiceAmounts = 7;

            while (choice != choiceAmounts)
            {
                Console.WriteLine($"{1}. Add book into library");
                Console.WriteLine($"{2}. Delete book from library");
                Console.WriteLine($"{3}. Show books in library");
                Console.WriteLine($"{4}. Borrow book");
                Console.WriteLine($"{5}. Display borrowed book information");
                Console.WriteLine($"{6}. Get book by id");
                Console.WriteLine($"{7}. Exit");
                Console.Write("Please choose an option: ");
                choice = int.Parse(Console.ReadLine()!);

                switch ( choice )
                {
                    case 1:
                        AddBookMenu();
                        break;
                    case 2:
                        DeleteBookMenu();
                        break;
                    case 3:
                        ShowBookListMenu();
                        break;
                    case 4:
                        LoanBookMenu();
                        break;
                    case 5:
                        GetLoanBookMenu();
                        break;
                    case 6:
                        GetBookByIdMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        private void AddBookMenu()
        {
            Console.Write("Please enter book title: ");
            string bookTitle = Console.ReadLine()!;
            Console.Write("Please enter book author: ");
            string bookAuthor = Console.ReadLine()!;
            Console.Write("Please enter stock quantity: ");
            int bookQuantity = int.Parse(Console.ReadLine()!);
            Console.Write("Please enter book price: ");
            decimal bookPrice = decimal.Parse(Console.ReadLine()!);

            Book newBook = new Book();
            newBook.Title = bookTitle;
            newBook.Author = bookAuthor;
            newBook.StockQuantity = bookQuantity;
            newBook.Price = bookPrice;

            abLibraryManager.AddBook(newBook);
        }

        private void ShowBookListMenu()
        {
            int choice = 0;
            int choiceAmounts = 3;
            int pageNumber = 1;
            int pageSize = 5;
            decimal totalPageNumbers = library.Books.Count() / pageSize;

            abLibraryManager.ShowBookList(pageNumber, pageSize);

            while (choice != choiceAmounts)
            {
                Console.WriteLine($"{1}. Next page");
                Console.WriteLine($"{2}. Previous page");
                Console.WriteLine($"{3}. Back to main menu");
                choice = int.Parse(Console.ReadLine()!);

                switch (choice)
                {
                    case 1:
                        if (pageNumber > Math.Ceiling(totalPageNumbers))
                        {
                            abLibraryManager.ShowBookList(pageNumber, pageSize);
                        }
                        else
                        {
                            pageNumber++;
                            abLibraryManager.ShowBookList(pageNumber, pageSize);
                        }
                        break;
                    case 2:
                        if (pageNumber < 1)
                        {
                            abLibraryManager.ShowBookList(pageNumber, pageSize);
                        }
                        else
                        {
                            pageNumber--;
                            abLibraryManager.ShowBookList(pageNumber, pageSize);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void DeleteBookMenu()
        {
            Console.Write("Please enter book id: ");
            int bookid = int.Parse(Console.ReadLine()!);

            abLibraryManager.DeleteBook(bookid);
        }

        private void LoanBookMenu()
        {
            CustomerManager customerManager = new CustomerManager();

            Console.Write("Please enter your name: ");
            string name = Console.ReadLine()!;
            Console.Write("Please enter book name: ");
            string bookName = Console.ReadLine()!;
            Console.Write("Please enter quantity: ");
            int quantity = int.Parse(Console.ReadLine()!);

            customerManager.BorrowBook(name, bookName, quantity);
            Console.WriteLine();
        }

        private void GetLoanBookMenu()
        {
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine()!;

            AbLoanManager abLoanManager = new LoanManager();

            abLoanManager.GetLoanBooks(name);
        }

        private void GetBookByIdMenu()
        {
            Console.Write("Please enter book id: ");
            int bookId = int.Parse(Console.ReadLine()!);

            abLibraryManager.GetBookById(bookId);
        }
    }
}
