﻿namespace LibraryManagement.Models
{
    public class Loan
    {
        public int Id { get; set; }
        public DateTime LoanDate { get; set; }
        public DateTime DueDate { get; set; }
        public Customer? Customer { get; set; }
        public List<Book>? Books { get; set; }
    }
}
