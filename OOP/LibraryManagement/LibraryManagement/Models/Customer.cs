﻿namespace LibraryManagement.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string? FullName { get; set; }
        public string? Address { get; set; }
        public List<Loan> Loans { get; set; } = new List<Loan>();

        public static List<Customer> customers = new List<Customer>
        {
            new Customer{ Id = 1, FullName = "Nguyen Van A", Address = "123 ABC"},
            new Customer{ Id = 2, FullName = "Nguyen Van B", Address = "456 ABC"},
            new Customer{ Id = 3, FullName = "Nguyen Van C", Address = "789 ABC"},
        };
    }
}
