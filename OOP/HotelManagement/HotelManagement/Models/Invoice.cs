﻿namespace HotelManagement.Models
{
    public class Invoice
    {
        private int _id;
        private decimal _totalPrice;
        private List<string>? _services;

        public int Id
        {
            get => _id; 
            set => _id = value;
        }

        public decimal TotalPrice
        {
            get => _totalPrice;
            set => _totalPrice = value;
        }

        public List<string> Services { get; set; } = new List<string>();
    }
}
