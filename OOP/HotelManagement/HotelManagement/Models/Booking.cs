﻿namespace HotelManagement.Models
{
    public class Booking
    {
        private int _id;
        private DateTime _bookedDate;
        private DateTime _checkInDate;
        private List<string>? _request;
        private Customer? _customer;

        public int Id 
        { 
            get => _id; 
            set => _id = value;
        }

        public DateTime BookedDate
        {
            get => _bookedDate;
            set => _bookedDate = value;
        }

        public DateTime CheckInDate
        {
            get => _checkInDate;
            set => _checkInDate = value;
        }

        public List<Room> BookedRooms { get; set; } = new List<Room>();

        public List<string> Request
        {
            get => _request!;
            set => _request = value;
        }
        
        public Customer Customer
        {
            get => _customer!;
            set => _customer = value;
        }

        /*public Booking(DateTime bookedDate, DateTime checkInDate, List<string> request)
        {
            BookedDate = bookedDate;
            CheckInDate = checkInDate;
            Request = request;
        }*/
    }
}
