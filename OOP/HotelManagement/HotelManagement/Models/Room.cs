﻿namespace HotelManagement.Models
{
    public class Room
    {
        private int _id;
        private string? _type;
        private decimal _pricePerNight;
        private bool _isAvailable;
        private List<string>? _facility;

        public int Id 
        { 
            get => _id; 
            set =>  _id = value; 
        
        }

        public string Type 
        { 
            get => _type!; 
            set => _type = value; 
        }

        public decimal PricePerNight 
        { 
            get => _pricePerNight; 
            set => _pricePerNight = value; 
        }

        public bool IsAvailable 
        { 
            get => _isAvailable; 
            set => _isAvailable = value; 
        }

        public List<string> Facility
        { 
            get => _facility!; 
            set => _facility = value; 
        }

        public Room(string type, decimal price, bool available, List<string> facility)
        {
            Type = type;
            PricePerNight = price;
            IsAvailable = available;
            Facility = facility;
        }
    }
}
