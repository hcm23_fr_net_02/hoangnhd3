﻿namespace HotelManagement.Models
{
    public class Customer
    {
        private int _id;
        private string? _fullName;
        private string? _phoneNumber;
        private string? _address;

        public int Id 
        { 
            get => _id; 
            set => _id = value; 
        }

        public string FullName 
        { 
            get => _fullName!; 
            set => _fullName = value; 
        }

        public string PhoneNumber 
        { 
            get => _phoneNumber!; 
            set => _phoneNumber = value; 
        }

        public string Address 
        { 
            get => _address!; 
            set => _address = value; 
        }

        public List<Invoice> Invoices { get; set; } = new List<Invoice>();

        public static List<Customer> customers = new List<Customer>
        {
            new Customer{ Id = 1, FullName = "Nguyen Van A", Address = "123 ABC", PhoneNumber = "0909009009"},
            new Customer{ Id = 2, FullName = "Nguyen Van B", Address = "456 ABC", PhoneNumber = "0911121314"},
            new Customer{ Id = 3, FullName = "Nguyen Van C", Address = "789 ABC", PhoneNumber = "0933445566"},
        };
    }
}
