﻿using HotelManagement.Models;
using HotelManagement.AbManagers;

namespace HotelManagement.Managers
{
    public class CustomerManager : AbCustomerManager
    {
        private Invoice invoice = new Invoice();
        private Booking booking = new Booking();
        int nextId = 1;

        public override void BookingRoom(string customerName, string roomType, 
                                         DateTime checkInDate, DateTime checkOutDate, 
                                         List<string> request)
        {
            var bookedCustomer = Customer.customers.FirstOrDefault(x => x.FullName == customerName);

            if (bookedCustomer is null)
            {
                throw new Exception("Customer not found!");
            }

            var bookedRoom = Hotel.Rooms.FirstOrDefault(x => x.Type == roomType && x.IsAvailable == true);

            if (bookedRoom is null)
            {
                throw new Exception("Rooms are full!");
            }
            else
            {
                var rentDate = (checkOutDate.Date - checkInDate.Date).Days;
                var totalPrice = bookedRoom.PricePerNight * rentDate;

                bookedRoom.IsAvailable = false;

                booking.Id = nextId++;
                booking.BookedDate = DateTime.Now;
                booking.CheckInDate = checkInDate;
                booking.Request = request;
                booking.BookedRooms.Add(bookedRoom);
                booking.Customer = bookedCustomer!;

                Hotel.Bookings.Add(booking);

                invoice.Id = nextId++;
                invoice.TotalPrice = totalPrice;
                invoice.Services = bookedRoom.Facility;

                bookedCustomer.Invoices.Add(invoice);
            }
        }
    }
}
