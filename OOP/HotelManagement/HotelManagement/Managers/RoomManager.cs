﻿using HotelManagement.Models;
using HotelManagement.AbManagers;


namespace HotelManagement.Managers
{
    public class RoomManager : AbRoomManager
    {
        int currentId = 1;

        public override Room AddRoom(Room room)
        {
            room.Id = currentId;
            Hotel.Rooms.Add(room);
            currentId++;
            return room;
        }

        public override List<Room> GetRooms(int pageNumber, int pageSize)
        {
            var rooms = Hotel.Rooms.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            foreach (var room in rooms)
            {
                Console.WriteLine($"Id: {room.Id}, Type: {room.Type}, Price Per Night: {room.PricePerNight}, Is Available: {room.IsAvailable}, Facilities: ");
                foreach (var facility in room.Facility)
                {
                    Console.Write($"{facility} ");
                }
                Console.WriteLine();
            }
            return rooms;
        }

        public override Room GetRoomById(int id)
        {
            var idRoom = Hotel.Rooms.FirstOrDefault(x => x.Id == id);
            return idRoom!;
        }

        public override void DeleteRoom(int id)
        {
            var deletedRoom = GetRoomById(id);
            Hotel.Rooms.Remove(deletedRoom);
        }
    }
}
