﻿using HotelManagement.Models;
using HotelManagement.AbManagers;

namespace HotelManagement.Managers
{
    public class BookingManager : AbBookingManager
    {
        private readonly Booking booking = new Booking();

        public override void GetBookedRooms()
        {
            foreach (var booking in Hotel.Bookings)
            {
                Console.WriteLine($"Id: {booking.Id}, Booked Date: {booking.BookedDate}, Check In Date: {booking.CheckInDate}, Request: ");
                foreach (var request in booking.Request)
                {
                    Console.WriteLine($"{request} ");
                }
            }
        }
    }
}
