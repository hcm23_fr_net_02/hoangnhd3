﻿using HotelManagement.Models;
using HotelManagement.AbManagers;

namespace HotelManagement.Managers
{
    public class InvoiceManager : AbInvoiceManager
    {
        Customer customer = new Customer();

        public override void GetInvoice(string name)
        {
            var invoiceCustomer = Customer.customers.FirstOrDefault(x => x.FullName == name);

            if (invoiceCustomer is null)
            {
                throw new Exception("User not found!");
            }

            foreach (var invoice in invoiceCustomer.Invoices)
            {
                Console.WriteLine($"Id: {invoice.Id}, Total Price: {invoice.TotalPrice}, Services: ");
                foreach (var service in invoice.Services)
                {
                    Console.Write($"{service} ");
                }
            }
        }
    }
}
