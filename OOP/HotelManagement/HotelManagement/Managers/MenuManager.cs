﻿using HotelManagement.Models;
using HotelManagement.AbManagers;

namespace HotelManagement.Managers
{
    public class MenuManager
    {
        AbRoomManager abRoomManager = new RoomManager();
        AbBookingManager abBookingManager = new BookingManager();

        public MenuManager(AbRoomManager _abRoomManager)
        {
            abRoomManager = _abRoomManager;
        }

        public void MainMenu()
        {
            int choice = 0;
            int choiceAmounts = 7;

            while (choice != choiceAmounts)
            {
                Console.WriteLine($"{1}. Add room");
                Console.WriteLine($"{2}. Delete room");
                Console.WriteLine($"{3}. Show rooms");
                Console.WriteLine($"{4}. Book room");
                Console.WriteLine($"{5}. Show booked room");
                Console.WriteLine($"{6}. Create and show invoice");
                Console.WriteLine($"{7}. Exit");
                Console.Write($"Please choose an option: ");
                choice = int.Parse(Console.ReadLine()!);

                if (choice < 0 || choice > choiceAmounts)
                {
                    throw new Exception("Invalid option!");
                }
                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        try
                        {
                            AddRoomMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 2:
                        Console.Clear();
                        try
                        {
                            DeleteRoomMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 3:
                        Console.Clear();
                        try
                        {
                            ShowRoomsMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 4:
                        Console.Clear();
                        try 
                        { 
                            BookingRoomMenu();
                        }
                        catch (Exception ex) 
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 5:
                        Console.Clear();
                        try
                        {
                            abBookingManager.GetBookedRooms();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 6:
                        Console.Clear();
                        try
                        {
                            GetInvoiceMenu();
                        }
                        catch (Exception ex) 
                        { 
                            Console.WriteLine(ex.Message); 
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void AddRoomMenu()
        {

            Console.Write("Please enter room type (Single, Double, King): ");
            string roomType = Console.ReadLine()!;

            Console.Write("Please enter room price: ");
            decimal roomPrice = decimal.Parse(Console.ReadLine()!);

            Console.Write("Please enter room facilities (separated by comma): ");
            string roomFacility = Console.ReadLine()!;

            List<string> roomFacilities = roomFacility.Split(',').Select(x => x.Trim()).ToList(); 

            Room newRoom = new Room(roomType, roomPrice, true, roomFacilities);

            abRoomManager.AddRoom(newRoom);
        }

        private void DeleteRoomMenu()
        {
            Console.Write("Please enter room id: ");
            int roomId = int.Parse(Console.ReadLine()!);

            abRoomManager.DeleteRoom(roomId);
        }

        private void ShowRoomsMenu()
        {
            Hotel hotel = new Hotel();
            int choice = 0;
            int choiceAmounts = 3;
            int pageNumber = 1;
            int pageSize = 5;
            decimal totalPageNumbers = Hotel.Rooms.Count() / pageSize;

            abRoomManager.GetRooms(pageNumber, pageSize);

            while (choice != choiceAmounts)
            {
                Console.WriteLine($"{1}. Next/{2}. Previous/{3}. Back");
                choice = int.Parse(Console.ReadLine()!);

                switch (choice)
                {
                    case 1:
                        Console.Clear();

                        if (pageNumber > Math.Ceiling(totalPageNumbers))
                        {
                            abRoomManager.GetRooms(pageNumber, pageSize);
                        }
                        else
                        {
                            pageNumber++;
                            abRoomManager.GetRooms(pageNumber, pageSize);
                        }

                        break;
                    case 2:
                        Console.Clear();

                        if (pageNumber < 1)
                        {
                            abRoomManager.GetRooms(pageNumber, pageSize);
                        }
                        else
                        {
                            pageNumber--;
                            abRoomManager.GetRooms(pageNumber, pageSize);
                        }

                        break;
                    default:
                        break;
                }
            }
        }

        private void BookingRoomMenu()
        {
            Console.Write("Please enter your name: ");
            string customerName = Console.ReadLine()!;

            Console.Write("Please enter room type (Single, Double, King): ");
            string roomType = Console.ReadLine()!;

            Console.Write("Please enter check-in date: ");
            DateTime checkInDate = DateTime.Parse(Console.ReadLine()!);

            Console.Write("Please enter check-out date: ");
            DateTime checkOutDate = DateTime.Parse(Console.ReadLine()!);

            Console.WriteLine("Please enter request (enter for skip): ");
            string request = Console.ReadLine()!;

            List<string> requests = request.Split(',').Select(x => x.Trim()).ToList();

            AbCustomerManager abCustomerManager = new CustomerManager();

            abCustomerManager.BookingRoom(customerName, roomType, checkInDate, checkOutDate, requests);
        }

        private void GetInvoiceMenu()
        {
            Console.Write("Please enter customer name: ");
            string customerName = Console.ReadLine()!;

            AbInvoiceManager abInvoiceManager = new InvoiceManager();

            abInvoiceManager.GetInvoice(customerName);
        }
    }
}
