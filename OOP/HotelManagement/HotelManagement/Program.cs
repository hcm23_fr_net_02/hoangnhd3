﻿using HotelManagement.Managers;
using HotelManagement.AbManagers;

AbRoomManager abRoomManager = new RoomManager();
MenuManager menuManager = new MenuManager(abRoomManager);
try
{
    menuManager.MainMenu();
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
