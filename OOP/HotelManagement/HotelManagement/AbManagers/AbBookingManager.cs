﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.AbManagers
{
    public abstract class AbBookingManager
    {
        public abstract void GetBookedRooms();
    }
}
