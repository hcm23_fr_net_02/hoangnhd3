﻿namespace HotelManagement.AbManagers
{
    public abstract class AbInvoiceManager
    {
        public abstract void GetInvoice(string name);
    }
}
