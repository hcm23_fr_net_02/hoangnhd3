﻿namespace HotelManagement.AbManagers
{
    public abstract class AbCustomerManager
    {
        public abstract void BookingRoom(string customerName, string roomType,
                                         DateTime checkInDate, DateTime checkOutDate,
                                         List<string> request);
    }
}
