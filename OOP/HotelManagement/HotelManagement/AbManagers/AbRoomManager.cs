﻿using HotelManagement.Models;

namespace HotelManagement.AbManagers
{
    public abstract class AbRoomManager
    {
        public abstract Room AddRoom(Room room);

        public abstract List<Room> GetRooms(int pageNumber, int pageSize);

        public abstract Room GetRoomById(int id);

        public abstract void DeleteRoom(int id);
    }
}
