﻿using PurchaseMobile.Models;
using System.Text;

namespace PurchaseMobile.Managers
{
    public abstract class AbStoreManager
    {
        public abstract MobilePhone AddPhone(MobilePhone mobilePhone);

        public abstract List<MobilePhone> GetPhoneList(int pageNumber, int pageSize);

        public abstract MobilePhone GetPhoneById(int id);

        public abstract void DeletePhone(int id);
    }

    public class StoreManager : AbStoreManager
    {
        Store store = new Store();
        private int currentId = 1;

        public override MobilePhone AddPhone(MobilePhone mobilePhone)
        {
            mobilePhone.Id = currentId;
            store.PhoneList.Add(mobilePhone);
            currentId++;
            return mobilePhone;
        }

        public override List<MobilePhone> GetPhoneList(int pageNumber, int pageSize)
        {
            var phoneList = store.PhoneList.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            foreach (var phone in phoneList)
            {
                Console.WriteLine($"Id: {phone.Id}, Phone Name: {phone.PhoneName}, Brand: {phone.Brand}, Quantity: {phone.StockQuantity}, Price: {phone.Price}");
            }
            return phoneList;
        }

        public override MobilePhone GetPhoneById(int id)
        {
            var idPhone = store.PhoneList.FirstOrDefault(x => x.Id == id);
            return idPhone!;
        }

        public override void DeletePhone(int id)
        {
            var deletedPhone  = GetPhoneById(id);
            store.PhoneList.Remove(deletedPhone);
        }
    }
}
