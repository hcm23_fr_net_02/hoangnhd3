﻿using PurchaseMobile.Models;

namespace PurchaseMobile.Managers
{
    public abstract class AbCustomerManager
    {
        public abstract MobilePhone PurchasePhone(string customerName, string name, int quantity);
    }

    public class CustomerManager : AbCustomerManager
    {
        private readonly Purchase purchase = new Purchase();
        private readonly Store store = new Store();
        private int nextId = 1;

        public CustomerManager(Purchase _purchase)
        {
            purchase = _purchase;
        }

        public override MobilePhone PurchasePhone(string customerName, string name, int quantity)
        {
            var purchasedCustomer = Customer.customers.FirstOrDefault(x => x.FullName == customerName);
            if ( purchasedCustomer is null)
            {
                Console.WriteLine("Customer not found!");
            }
            var purchasePhone = store.PhoneList.FirstOrDefault(x => x.PhoneName == name);
            if (purchasePhone is null)
            {
                Console.WriteLine("Phone not found!");
            }

            if (quantity <= purchasePhone!.StockQuantity)
            {
                purchase.Id = nextId++;
                purchase.PurchaseDate = DateTime.Now;
                purchase.Customer = purchasedCustomer!;

                for (int i = 0; i < quantity; i++)
                {
                    purchase.PurchasedPhones.Add(purchasePhone);
                }

                purchasePhone!.StockQuantity -= quantity;

                purchasedCustomer!.Purchases.Add(purchase);
            }
            else
            {
                Console.WriteLine("Cannot buy more phone!");
            }
            return purchasePhone;
        }
    }
}
