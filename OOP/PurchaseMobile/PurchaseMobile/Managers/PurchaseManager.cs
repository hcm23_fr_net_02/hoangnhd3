﻿using PurchaseMobile.Models;
using System.Numerics;
using System.Text;

namespace PurchaseMobile.Managers
{
    public abstract class AbPurchaseManager
    {
        public abstract void GetPurchaseList(string name);
    }


    public class PurchaseManager : AbPurchaseManager
    {
        public override void GetPurchaseList(string name)
        {
            var purchasedCustomer = Customer.customers.FirstOrDefault(x => x.FullName == name);

            foreach (var purchase in purchasedCustomer!.Purchases)
            {
                Console.WriteLine($"Id: {purchase.Id}, Purchased Date: {purchase.PurchaseDate}, Phone List: ");

                foreach (var phone in purchase.PurchasedPhones)
                {
                    Console.WriteLine($"- Phone Name: {phone.PhoneName}, Brand: {phone.Brand}, Price: {phone.Price}");
                }
            }
        }
    }
}
