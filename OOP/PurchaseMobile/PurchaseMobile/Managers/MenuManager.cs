﻿using PurchaseMobile.Models;

namespace PurchaseMobile.Managers
{
    public class MenuManager
    {
        AbStoreManager abStoreManager = new StoreManager();
        Store store = new Store();

        public MenuManager(AbStoreManager _abStoreManager)
        {
            abStoreManager = _abStoreManager;
        }

        public void MainMenu()
        {
            int choice = 0;
            int choiceAmounts = 7;

            while (choice != choiceAmounts)
            {
                Console.WriteLine($"{1}. Add phone into store");
                Console.WriteLine($"{2}. Delete phone from store");
                Console.WriteLine($"{3}. Show phones in store");
                Console.WriteLine($"{4}. Buy phone");
                Console.WriteLine($"{5}. Show purchase");
                Console.WriteLine($"{6}. Get phone by id");
                Console.WriteLine($"{7}. Exit");
                Console.Write("Please choose an option: ");
                choice = int.Parse(Console.ReadLine()!);

                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        try
                        {
                            AddPhoneMenu();
                        }
                        catch (Exception ex) 
                        {
                            Console.WriteLine(ex.Message);
                        }
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        DeletePhoneMenu();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        try
                        {
                            GetPhonesListMenu();
                        }
                        catch (Exception ex) 
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 4:
                        Console.Clear();
                        try
                        {
                            PurchasePhoneMenu();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 5:

                        Console.Clear();
                        try
                        {
                            GetPurchaseMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 6:
                        Console.Clear();
                        GetPhoneByIdMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        private void AddPhoneMenu()
        {
            Console.Write("Please enter phone's name: ");
            string phoneName = Console.ReadLine()!;
            Console.Write("Please enter phone's brand: ");
            string phoneBrand = Console.ReadLine()!;
            Console.Write("Please enter phone's quantity: ");
            int quantity = int.Parse(Console.ReadLine()!);
            Console.Write("Please enter phone's price: ");
            decimal phonePrice = decimal.Parse(Console.ReadLine()!);

            MobilePhone phone = new MobilePhone();
            phone.PhoneName = phoneName;
            phone.Brand = phoneBrand;
            phone.StockQuantity = quantity;
            phone.Price = phonePrice;

            abStoreManager.AddPhone(phone);
        }

        private void GetPhonesListMenu()
        {
            int choice = 0;
            int choiceAmounts = 3;
            int pageNumber = 1;
            int pageSize = 5;
            decimal totalPageNumbers = store.PhoneList.Count() / pageSize;

            abStoreManager.GetPhoneList(pageNumber, pageSize);

            while (choice != choiceAmounts)
            {
                Console.WriteLine($"{1}. Next page");
                Console.WriteLine($"{2}. Previous page");
                Console.WriteLine($"{3}. Exit");
                Console.Write("Please choose an option: ");
                choice = int.Parse(Console.ReadLine()!);

                switch (choice)
                {
                    case 1:
                        if (pageNumber > Math.Ceiling(totalPageNumbers))
                        {
                            abStoreManager.GetPhoneList(pageNumber, pageSize);
                        }
                        else
                        {
                            pageNumber++;
                            abStoreManager.GetPhoneList(pageNumber, pageSize);
                        }
                        break;
                    case 2:
                        if (pageNumber > 1)
                        {
                            pageNumber--;
                            abStoreManager.GetPhoneList(pageNumber, pageSize);
                        }
                        else
                        {
                            abStoreManager.GetPhoneList(pageNumber, pageSize);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void DeletePhoneMenu()
        {
            Console.Write("Please enter phone id: ");
            int phoneId = int.Parse(Console.ReadLine()!);

            abStoreManager.DeletePhone(phoneId);
        }

        private void PurchasePhoneMenu()
        {
            Console.Write("Please enter your name: ");
            string customerName = Console.ReadLine()!;
            Console.Write("Please enter phone's name: ");
            string phoneName = Console.ReadLine()!;
            Console.Write("Please enter quantity: ");
            int quantity = int.Parse(Console.ReadLine()!);

            AbCustomerManager abcustomerManager = new CustomerManager(new Purchase());
            abcustomerManager.PurchasePhone(customerName, phoneName, quantity);
        }

        private void GetPurchaseMenu()
        {
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine()!;

            AbPurchaseManager abPurchaseManager = new PurchaseManager();

            abPurchaseManager.GetPurchaseList(name);
            Console.WriteLine();
        }

        private void GetPhoneByIdMenu()
        {
            Console.WriteLine("Please enter phone's id: ");
            int phoneId = int.Parse(Console.ReadLine()!);

            abStoreManager.GetPhoneById(phoneId);
        }
    }
}
