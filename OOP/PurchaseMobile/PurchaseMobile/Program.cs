﻿using PurchaseMobile.Managers;

StoreManager storeManager = new StoreManager();
MenuManager menuManager = new MenuManager(storeManager);
try
{
    menuManager.MainMenu();
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
