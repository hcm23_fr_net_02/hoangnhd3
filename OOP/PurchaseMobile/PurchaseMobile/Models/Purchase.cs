﻿namespace PurchaseMobile.Models
{
    public class Purchase
    {
        public int Id { get; set; }
        public DateTime PurchaseDate { get; set; }
        public List<MobilePhone> PurchasedPhones { get; set; }
        public Customer Customer { get; set; }

        public Purchase()
        {
            PurchasedPhones = new List<MobilePhone>();
        }
    }
}
