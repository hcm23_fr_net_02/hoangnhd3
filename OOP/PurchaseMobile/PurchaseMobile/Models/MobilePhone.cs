﻿namespace PurchaseMobile.Models
{
    public class MobilePhone
    {
        public int Id { get; set; }

        public string? PhoneName { get; set; }

        public string? Brand { get; set; }

        private int _StockQuantity;

        public int StockQuantity
        {
            get { return _StockQuantity; }
            set { _StockQuantity = value; }
        }

        private decimal _Price;

        public decimal Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

    }
}
