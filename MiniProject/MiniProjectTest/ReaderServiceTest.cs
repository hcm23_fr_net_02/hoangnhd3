﻿using MiniProject.Contracts.IRepository;
using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;
using MiniProject.Services;

namespace MiniProjectTest
{
    public class ReaderServiceTest
    {
        [Fact]
        public void AddReader_ShouldAddReaderSuccessfully()
        {
            //Arrange
            var readerRepositoryMock = new Mock<IReaderRepository>();
            var readerService = new ReaderService(readerRepositoryMock.Object);

            string name = "Di Hon";
            string address = "1 Le Duan";
            string phone = "999 999 9999";

            readerRepositoryMock.Setup(repo => repo.Add(It.IsAny<Reader>())).Callback<Reader>(reader =>
            {
                Assert.Equal(name, reader.FullName);
                Assert.Equal(address, reader.Address);
                Assert.Equal(phone, reader.PhoneNumber);
            });

            //Act
            Reader addedReader = readerService.AddReader(name, address, phone);

            //Assert
            readerRepositoryMock.Verify(repo => repo.Add(It.IsAny<Reader>()), Times.Once);
            Assert.NotNull(addedReader);
        }

        [Fact]
        public void UpdateReader_ShouldUpdateReaderSuccessfully()
        {
            //Arrange
            var readerRepositoryMock = new Mock<IReaderRepository>();
            var readerService = new ReaderService(readerRepositoryMock.Object);

            int readerId = 1;
            string name = "B";
            string address = "1 B Street";
            string phoneNumber = "111 111 1111";

            readerRepositoryMock.Setup(repo => repo.UpdateReader(It.IsAny<int>(), It.IsAny<DTOReader>()))
                                .Callback<int, DTOReader>((id, dtoReader) =>
                                {
                                    Assert.Equal(readerId, id);
                                    Assert.Equal(name, dtoReader.FullName);
                                    Assert.Equal(address, dtoReader.Address);
                                    Assert.Equal(phoneNumber, dtoReader.PhoneNumber);
                                });

            //Act
            DTOReader updatedReader = readerService.UpdateReader(readerId, name, address, phoneNumber);

            //Assert
            readerRepositoryMock.Verify(repo => repo.UpdateReader(readerId, It.IsAny<DTOReader>()), Times.Once);

            Assert.NotNull(updatedReader);
        }

        [Fact]
        public void ReaderListWithDetail_ShouldReturnExpectedReaders()
        {
            //Arrange
            var readerRepositoryMock = new Mock<IReaderRepository>();
            var readerService = new ReaderService(readerRepositoryMock.Object);

            int pageNumber = 2;
            int pageSize = 5;

            List<Reader> expectedReaders = new List<Reader>
            {
                new Reader { Id = 6, FullName = "Charmine Tranter", Address = "94676 Trailsway Circle", PhoneNumber = "862 700 7480" },
                new Reader { Id = 7, FullName = "Marjory Laba", Address = "4 Chive Circle", PhoneNumber = "722 654 0090" },
                new Reader { Id = 8, FullName = "Kerrill Delle", Address = "7020 Lakewood Hill", PhoneNumber = "726 178 6683" },
                new Reader { Id = 9, FullName = "Thornton Lukovic", Address = "2 Fulton Terrace", PhoneNumber = "973 176 8498" },
                new Reader { Id = 10, FullName = "Janna Cahen", Address = "27235 Southridge Hill", PhoneNumber = "379 976 6320" }
            };

            readerRepositoryMock.Setup(repo => repo.Pagination(pageNumber, pageSize)).Returns(expectedReaders); 

            //Act
            List<Reader> actualReaders = readerService.ReaderListWithDetail(pageNumber, pageSize);

            //Assert
            readerRepositoryMock.Verify(repo => repo.Pagination(pageNumber, pageSize), Times.Once);

            Assert.Equal(expectedReaders, actualReaders);
        }
    }
}
