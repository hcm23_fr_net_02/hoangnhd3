﻿using MiniProject.Contracts.IRepository;
using MiniProject.Models.DomainModels;
using MiniProject.Services;

namespace MiniProjectTest
{
    public class ReviewServiceTest
    {
        [Fact]
        public void ReviewListWithDetail_ReturnExpectedReviews()
        {
            //Arrange
            var reviewRepositoryMock = new Mock<IReviewRepository>();
            var reviewService = new ReviewService(reviewRepositoryMock.Object);

            int pageNumber = 2;
            int pageSize = 3;
            int bookId = 7;

            List<Review> expectedReviews = new List<Review>
            {
                new Review { Id = 10, BookId = 2, Title = "suspendisse potenti nullam", Description = "ipsum dolor sit amet consectetuer adipiscing", Rating = 4 },
                new Review { Id = 71, BookId = 2, Title = "ante vel ipsum", Description = "convallis duis consequat dui nec nisi", Rating = 5 },
            };

            reviewRepositoryMock.Setup(repo => repo.Pagination(pageNumber, pageSize, bookId)).Returns(expectedReviews);

            //Act
            List<Review> actualReviews = reviewService.ReviewListWithDetail(pageNumber, pageSize, bookId);

            //Assert
            reviewRepositoryMock.Verify(repo => repo.Pagination(pageNumber, pageSize, bookId));

            Assert.Equal(expectedReviews, actualReviews);
        }

        [Fact]
        public void AddReview_ShouldAddReviewSuccessfully()
        {
            //Arrange
            var reviewRepositoryMock = new Mock<IReviewRepository>();
            var reviewService = new ReviewService(reviewRepositoryMock.Object);

            string title = "Good";
            string description = "What a good book";
            int rating = 5;

            reviewRepositoryMock.Setup(repo => repo.Add(It.IsAny<Review>())).Callback<Review>(review =>
            {
                Assert.Equal(title, review.Title);
                Assert.Equal(description, review.Description);
                Assert.Equal(rating, review.Rating);
            });

            //Act
            Review addedReview = reviewService.AddReview(title, description, rating);

            //Assert
            reviewRepositoryMock.Verify(repo => repo.Add(It.IsAny<Review>()), Times.Once);

            Assert.NotNull(addedReview);
        }
    }
}
