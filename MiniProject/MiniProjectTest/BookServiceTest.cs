﻿using MiniProject.Contracts.IRepository;
using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;
using MiniProject.Services;

namespace MiniProjectTest
{
    public class BookServiceTest
    {
        [Fact]
        public void AddBook_ShouldAddBookSuccessfully()
        {
            // Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object);

            string title = "ABC";
            string author = "Di Hon";
            string genre = "Comedy";
            int publicationYear = 2023;
            int stockQuantity = 5;
            bool isAvailable = true;

            bookRepositoryMock.Setup(repo => repo.Add(It.IsAny<Book>())).Callback<Book>(book =>
            {
                Assert.Equal(title, book.Title);
                Assert.Equal(author, book.Author);
                Assert.Equal(genre, book.Genre);
                Assert.Equal(publicationYear, book.PublicationYear);
                Assert.Equal(stockQuantity, book.StockQuantity);
                Assert.Equal(isAvailable, book.IsAvailable);
            });

            // Act
            Book addedBook = bookService.AddBook(title, author, genre, publicationYear, stockQuantity);

            // Assert
            bookRepositoryMock.Verify(repo => repo.Add(It.IsAny<Book>()), Times.Once);

            Assert.NotNull(addedBook);
        }

        [Fact]
        public void UpdateBook_ShouldUpdateBookSuccessfully()
        {
            //Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object);

            int bookId = 2;
            string title = "B";
            string author = "B";
            string genre = "War";
            int publicationYear = 2023;
            int quantity = 0;
            bool isAvailable = false;

            bookRepositoryMock.Setup(repo => repo.UpdateBook(It.IsAny<int>(), It.IsAny<DTOBook>()))
                              .Callback<int, DTOBook>((id, dtoBook) =>
                              {
                                  Assert.Equal(bookId, id);
                                  Assert.Equal(title, dtoBook.Title);
                                  Assert.Equal(author, dtoBook.Author);
                                  Assert.Equal(genre, dtoBook.Genre);
                                  Assert.Equal(publicationYear, dtoBook.PublicationYear);
                                  Assert.Equal(quantity, dtoBook.Quantity);
                                  Assert.Equal(isAvailable, dtoBook.IsAvailable);
                              });
            //Act
            DTOBook updatedBook = bookService.UpdateBook(bookId, title, author, genre, publicationYear, quantity, isAvailable);

            //Assert
            bookRepositoryMock.Verify(repo => repo.UpdateBook(bookId, It.IsAny<DTOBook>()), Times.Once);

            Assert.NotNull(updatedBook);
        }

        [Fact]
        public void BookListWithDetails_ShouldReturnBooksWithDetails()
        {
            //Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object);

            int pageNumber = 2;
            int pageSize = 5;

            List<Book> expectedBooks = new List<Book>
            {
                new Book { Id = 6, Title = "Herr Lehmann", Author = "Helenka Goscar", Genre = "Drama", PublicationYear = 2020, StockQuantity = 5, IsAvailable = true },
                new Book { Id = 7, Title = "Bordertown", Author = "Jemmy Gilson", Genre = "Thriller", PublicationYear = 2015, StockQuantity = 0, IsAvailable = false },
                new Book {Id = 8, Title = "Ironclad 2: Battle for Blood", Author = "Terese Gorse", Genre = "Action", PublicationYear = 2008, StockQuantity = 0, IsAvailable = false },
                new Book {Id = 9, Title = "Ghost and the Darkness, The", Author = "Tamma Pester", Genre = "Adventure", PublicationYear = 2010, StockQuantity = 2, IsAvailable = true },
                new Book {Id = 10, Title = "Budz House", Author = "Roland Jemmison", Genre = "Comedy", PublicationYear = 2003, StockQuantity = 0, IsAvailable = false },
            };
            bookRepositoryMock.Setup(service => service.Pagination(pageNumber, pageSize)).Returns(expectedBooks);

            //Act
            List<Book> actualBooks = bookService.BookListWithDetails(pageNumber, pageSize);

            //Assert
            bookRepositoryMock.Verify(service => service.Pagination(pageNumber, pageSize));

            Assert.Equal(expectedBooks, actualBooks);
        }

        [Fact]
        public void BookByIdWithDetails_ShouldReturnBookWithDetail()
        {
            //Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object);

            int bookId = 17;

            Book expectedBook = new Book
            {
                Id = bookId,
                Title = "Fat Head",
                Author = "Cinnamon Kinnoch",
                Genre = "Documentary",
                PublicationYear = 2023,
                StockQuantity = 5,
                IsAvailable = true,
            };

            bookRepositoryMock.Setup(repo => repo.GetById(bookId)).Returns(expectedBook);

            //Act
            Book actualBook = bookService.BookByIdWithDetails(bookId);

            //Assert
            bookRepositoryMock.Verify(repo => repo.GetById(bookId), Times.Once);
            Assert.Equal(expectedBook, actualBook);
        }

        [Fact]
        public void FilterAuthorWithDetail_ShouldReturnBookListByAuthor()
        {
            //Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object);

            string author = "Otis Ashman";

            List<Book> expectedBooks = new List<Book>
            {
                new Book {Id = 34, Title = "Septien", Author = "Otis Ashman", Genre = "Drama", PublicationYear = 2001, StockQuantity = 3, IsAvailable = true },
                new Book {Id = 36, Title = "How They Get There", Author = "Otis Ashman", Genre = "Drama", PublicationYear = 2014, StockQuantity = 2, IsAvailable = true }
            };

            bookRepositoryMock.Setup(repo => repo.FilterAuthor(author)).Returns(expectedBooks);

            //Act
            List<Book> actualBooks = bookService.FilterAuthorWithDetail(author);

            //Assert
            bookRepositoryMock.Verify(repo => repo.FilterAuthor(author), Times.Once);
            Assert.Equal(expectedBooks, actualBooks);
        }

        [Fact]
        public void FilterTitleWithDetail_ShouldReturnBookListByTitle()
        {
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object);

            string title = "Kill";

            List<Book> expectedBooks = new List<Book>
            {
                new Book { Id = 24, Title = "I Know Who Killed Me", Author = "Freeland Pipet", Genre = "Crime", PublicationYear = 2000, StockQuantity = 2, IsAvailable = true },
                new Book { Id = 25, Title = "Who Do I Gotta Kill?", Author = "Sharyl Kissock", Genre = "Comedy", PublicationYear = 2015, StockQuantity = 2, IsAvailable = true },
                new Book { Id = 26, Title = "Jack the Giant Killer", Author = "Si Sturm", Genre = "Fantasy", PublicationYear = 2013, StockQuantity = 4, IsAvailable = true },
                new Book { Id = 30, Title = "Act of Killing, The", Author = "Cinnamon Kinnoch", Genre = "Documentary", PublicationYear = 2015, StockQuantity = 3, IsAvailable = true }
            };

            bookRepositoryMock.Setup(repo => repo.FilterTitle(title)).Returns(expectedBooks);

            //Act
            List<Book> actualBooks = bookService.FilterTitleWithDetail(title);

            //Assert
            bookRepositoryMock.Verify(repo => repo.FilterTitle(title), Times.Once);
            Assert.Equal(expectedBooks, actualBooks);
        }

        [Fact]
        public void FilterGenreWithDetail_ShouldReturnBookListByGenre()
        {
            //Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object);

            string genre = "War";

            List<Book> expectedBooks = new List<Book>
            {
                new Book { Id = 1, Title = "Invisible War, The", Author = "Tamma Pester", Genre = "War", PublicationYear = 2001, StockQuantity = 3, IsAvailable = true },
                new Book { Id = 3, Title = "Castle Keep", Author = "Si Sturm", Genre = "War", PublicationYear = 2004, StockQuantity = 0, IsAvailable = false },
                new Book { Id = 27, Title = "Devil's Brigade, The", Author = "Si Sturm", Genre = "War", PublicationYear = 2014, StockQuantity = 2, IsAvailable = true }
            };

            bookRepositoryMock.Setup(repo => repo.FilterGenre(genre)).Returns(expectedBooks);

            //Act
            List<Book> actualBooks = bookService.FilterGenreWithDetail(genre);

            //Assert
            bookRepositoryMock.Verify(repo => repo.FilterGenre(genre));
            Assert.Equal(expectedBooks, actualBooks);
        }

        [Fact]
        public void FilterPublicationYearWithDetail_ShouldReturnBookListByYear()
        {
            //Arrange
            var bookRepositoryMock = new Mock<IBookRepository>();
            var bookService = new BookService(bookRepositoryMock.Object);

            int year = 2000;

            List<Book> expectedBooks = new List<Book>
            {
                new Book { Id = 14, Title = "Speed & Angels", Author = "Aliza Larman", Genre = "Drama", PublicationYear = 2008, StockQuantity = 1, IsAvailable = true },
                new Book { Id = 24, Title = "I Know Who Killed Me", Author = "Freeland Pipet", Genre = "Crime", PublicationYear = 2000, StockQuantity = 2, IsAvailable = true },
                new Book { Id = 35, Title = "Pretty Things", Author = "Aliza Larman", Genre = "Drama", PublicationYear = 2000, StockQuantity = 5, IsAvailable = true },
            };

            bookRepositoryMock.Setup(repo => repo.FilterPublicationYear(year)).Returns(expectedBooks);

            //Act
            var actualBooks = bookService.FilterYearWithDetail(year);

            //Assert
            bookRepositoryMock.Verify(repo => repo.FilterPublicationYear(year), Times.Once);
            Assert.Equal(expectedBooks, actualBooks);
        }
    }
}
