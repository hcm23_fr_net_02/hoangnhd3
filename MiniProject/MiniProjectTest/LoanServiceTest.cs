﻿using MiniProject.Contracts.IRepository;
using MiniProject.Models.DomainModels;
using MiniProject.Services;

namespace MiniProjectTest
{
    public class LoanServiceTest
    {
        [Fact]
        public void BorrowBook_UpdatesLoanAndStockQuantity()
        {
            var bookId = 1;
            var readerName = "Adele Gudd";
            var loanQuantity = 3;
            var dueDate = DateTime.Now.AddDays(14);

            var bookRepositoryMock = new Mock<IBookRepository>();
            var readerRepositoryMock = new Mock<IReaderRepository>();
            var loanRepositoryMock = new Mock<ILoanRepository>();
            var loanDetailRepositoryMock = new Mock<ILoanDetailRepository>();

            var loanService = new LoanService(loanRepositoryMock.Object, loanDetailRepositoryMock.Object, bookRepositoryMock.Object, readerRepositoryMock.Object);

            var reader = new Reader { Id = 1, FullName = readerName, Address = "98 Mapple Crossing", PhoneNumber = "870 246 3864" };
            var book = new Book { Id = 1, Title = "Invisible War, The", Author = "Tamma Pester", Genre = "War", PublicationYear = 2001, StockQuantity = 3, IsAvailable = true };

            readerRepositoryMock.Setup(r => r.GetReaderByName(readerName)).Returns(reader);
            bookRepositoryMock.Setup(b => b.GetById(bookId)).Returns(book);

            //Act
            loanService.BorrowBook(bookId, readerName, loanQuantity, dueDate);

            //Assert
            Assert.Equal(0, book.StockQuantity);
            Assert.False(book.IsAvailable);
            loanRepositoryMock.Verify(repo => repo.Add(It.IsAny<Loan>()), Times.Once);
            loanDetailRepositoryMock.Verify(repo => repo.Add(It.IsAny<LoanDetail>()), Times.Once);
        }
    }
}
