﻿using MiniProject.Models.DomainModels;
using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Utils;

namespace MiniProject.ConsoleMenu
{
    public class ReviewMenu
    {
        private readonly IReviewRepository _reviewRepository;
        private readonly IReviewService _reviewService;

        public ReviewMenu(IReviewRepository reviewRepository, IReviewService reviewService)
        {
            _reviewRepository = reviewRepository;
            _reviewService = reviewService;
        }

        public void AddReviewMenu()
        {
            Console.Write(Constants.EnterReviewTitle);
            string title = Console.ReadLine()!;

            Console.Write(Constants.EnterReviewDesc);
            string description = Console.ReadLine()!;

            Console.Write(Constants.EnterReviewRating);
            int rating = int.Parse(Console.ReadLine()!);

            _reviewService.AddReview(title, description, rating);
        }

        public void ShowReviewListMenu(int bookId)
        {
            bool quit = false;
            int pageNumber = 1;
            int pageSize = 5;
            decimal totalPageNumbers = _reviewRepository.GetAll().Where(x => x.BookId == bookId).Count() / pageSize;

            _reviewService.ReviewListWithDetail(pageNumber, pageSize, bookId);

            while (!quit)
            {
                Console.WriteLine(Constants.Pagination1);
                ConsoleKeyInfo choice = Console.ReadKey()!;

                if (choice.Key == ConsoleKey.N || choice.Key == ConsoleKey.P || choice.Key == ConsoleKey.B)
                {
                    switch (choice.Key)
                    {
                        case ConsoleKey.N:
                            if (pageNumber > Math.Ceiling(totalPageNumbers))
                            {
                                Console.Clear();
                                _reviewService.ReviewListWithDetail(pageNumber, pageSize, bookId);
                            }
                            else
                            {
                                pageNumber++;
                                Console.Clear();
                                _reviewService.ReviewListWithDetail(pageNumber, pageSize, bookId);
                            }

                            break;
                        case ConsoleKey.P:
                            if (pageNumber == 1)
                            {
                                Console.Clear();
                                _reviewService.ReviewListWithDetail(pageNumber, pageSize, bookId);
                            }
                            else
                            {
                                pageNumber--;
                                Console.Clear();
                                _reviewService.ReviewListWithDetail(pageNumber, pageSize, bookId);
                            }

                            break;
                        default:
                            quit = true;
                            Console.Clear();
                            break;
                    }
                }
                else
                {
                    throw new Exception(Constants.InvalidKey);
                }
            }
        }
    }
}
