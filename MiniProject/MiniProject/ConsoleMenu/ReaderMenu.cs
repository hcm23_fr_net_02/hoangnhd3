﻿using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;
using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Utils;

namespace MiniProject.ConsoleMenu
{
    public class ReaderMenu
    {
        private readonly IReaderRepository _readerRepository;
        private readonly IReaderService _readerService;
        public ReaderMenu(IReaderRepository readerRepository, IReaderService readerService)
        {
            _readerRepository = readerRepository;
            _readerService = readerService;
        }

        public void ReaderManagementMenu()
        {
            int choice = 0;
            int choiceAmounts = 5;

            while (choice != choiceAmounts)
            {
                HardMenu.MenuReader();
                choice = int.Parse(Console.ReadLine()!);

                if (choice < 1 || choice > choiceAmounts)
                {
                    throw new Exception(Constants.InvalidOption);
                }

                switch (choice)
                {
                    case 1:
                        Console.Clear();

                        try
                        {
                            AddReaderMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 2:
                        Console.Clear();

                        try
                        {
                            ShowReaderListMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 3:
                        try
                        {
                            UpdateReaderMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        Console.Clear();
                        break;
                    case 4:
                        Console.Clear();

                        try
                        {
                            DeleteReaderMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }

        public void AddReaderMenu()
        {
            Console.Write(Constants.EnterReaderName);
            string readerName = Console.ReadLine()!;

            Console.Write(Constants.EnterReaderAddress);
            string readerAddress = Console.ReadLine()!;

            Console.Write(Constants.EnterReaderNumber);
            string readerPhoneNumber = Console.ReadLine()!;

            _readerService.AddReader(readerName, readerAddress, readerPhoneNumber);
        }

        public void ShowReaderListMenu()
        {
            bool quit = false;
            int pageNumber = 1;
            int pageSize = 5;
            decimal totalPageNumbers = _readerRepository.GetAll().Count / pageSize;

            _readerService.ReaderListWithDetail(pageNumber, pageSize);

            while(!quit)
            {
                Console.WriteLine(Constants.Pagination1);
                ConsoleKeyInfo choice = Console.ReadKey()!;

                if (choice.Key == ConsoleKey.N || choice.Key == ConsoleKey.P || choice.Key == ConsoleKey.B)
                {
                    switch (choice.Key)
                    {
                        case ConsoleKey.N:
                            if (pageNumber > Math.Ceiling(totalPageNumbers))
                            {
                                Console.Clear();
                                _readerService.ReaderListWithDetail(pageNumber, pageSize);
                            }
                            else
                            {
                                pageNumber++;
                                Console.Clear();
                                _readerService.ReaderListWithDetail(pageNumber, pageSize);
                            }

                            break;
                        case ConsoleKey.P:
                            if (pageNumber == 1)
                            {
                                Console.Clear();
                                _readerService.ReaderListWithDetail(pageNumber, pageSize);
                            }
                            else
                            {
                                pageNumber--;
                                Console.Clear();
                                _readerService.ReaderListWithDetail(pageNumber, pageSize);
                            }

                            break;
                        default:
                            quit = true;
                            Console.Clear();
                            break;
                    }
                }
                else
                {
                    throw new Exception(Constants.InvalidKey);
                }
            }
        }

        public void UpdateReaderMenu()
        {
            Console.Write(Constants.EnterReaderId);
            int readerId = int.Parse(Console.ReadLine()!);

            Console.Write(Constants.EnterReaderName);
            string readerName = Console.ReadLine()!;

            Console.Write(Constants.EnterReaderAddress);
            string readerAddress = Console.ReadLine()!;

            Console.Write(Constants.EnterReaderNumber);
            string readerPhoneNumber = Console.ReadLine()!;

            _readerService.UpdateReader(readerId, readerName, readerAddress, readerPhoneNumber);
        }

        public void DeleteReaderMenu()
        {
            Console.Write(Constants.EnterReaderId);
            int readerId = int.Parse(Console.ReadLine()!);

            _readerRepository.Delete(readerId);
        }
    }
}
