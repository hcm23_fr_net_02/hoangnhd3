﻿using MiniProject.Utils;

namespace MiniProject.ConsoleMenu
{
    public class MainMenu
    {
        private readonly BookMenu _bookMenu;
        private readonly ReaderMenu _readerMenu;
        private readonly FilterMenu _filterMenu;
        private readonly LoanMenu _loanMenu;

        public MainMenu(BookMenu bookMenu, ReaderMenu readerMenu, FilterMenu filterMenu, LoanMenu loanMenu)
        {
            _bookMenu = bookMenu;
            _readerMenu = readerMenu;
            _filterMenu = filterMenu;
            _loanMenu = loanMenu;
        }

        public void MainMenuManager()
        {
            int choice = 0;
            int choiceAmounts = 6;

            while (choice != choiceAmounts)
            {
                HardMenu.MenuMain();
                choice = int.Parse(Console.ReadLine()!);

                switch (choice)
                {
                    case 1:
                        Console.Clear();

                        try
                        {
                            _bookMenu.BookListMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 2:
                        Console.Clear();

                        try
                        {
                            _bookMenu.AddBookMenu();

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 3:
                        Console.Clear();

                        try
                        {
                            _filterMenu.FilterManagementMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 4:
                        Console.Clear();

                        try
                        {
                            _loanMenu.LoanBookListMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 5:
                        Console.Clear();

                        try
                        {
                            _readerMenu.ReaderManagementMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    default:
                        break;
                }
            }
        }
    }
}
