﻿using MiniProject.Contracts.IService;
using MiniProject.Utils;

namespace MiniProject.ConsoleMenu
{
    public class FilterMenu
    {
        private readonly IBookService _bookService;

        public FilterMenu(IBookService bookService)
        {
            _bookService = bookService;
        }

        public void FilterManagementMenu()
        {
            int choice = 0;
            int choiceAmounts = 5;

            while (choice != choiceAmounts)
            {
                HardMenu.MenuFilter();
                choice = int.Parse(Console.ReadLine()!);

                if (choice < 1 || choice > choiceAmounts)
                {
                    throw new Exception(Constants.InvalidOption);
                }

                switch (choice)
                {
                    case 1:
                        Console.Clear();

                        try
                        {
                            FilterByTitleMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 2:
                        Console.Clear();

                        try
                        {
                            FilterByAuthorMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 3:
                        Console.Clear();

                        try
                        {
                            FilterByGenreMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 4:
                        Console.Clear();

                        try
                        {
                            FilterByYearMenu();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    default:
                        break;
                }
            }
        }

        public void FilterByTitleMenu()
        {
            Console.Write(Constants.EnterBookTitle);
            string bookTitle = Console.ReadLine()!;

            _bookService.FilterTitleWithDetail(bookTitle);
        }

        public void FilterByAuthorMenu()
        {
            Console.Write(Constants.EnterBookAuthor);
            string bookAuthor = Console.ReadLine()!;

            _bookService.FilterAuthorWithDetail(bookAuthor);
        }

        public void FilterByGenreMenu()
        {
            Console.Write(Constants.EnterBookGenre);
            string bookGenre = Console.ReadLine()!;

            _bookService.FilterGenreWithDetail(bookGenre);
        }

        public void FilterByYearMenu()
        {
            Console.Write(Constants.EnterBookPublicationYear);
            int publicationYear = int.Parse(Console.ReadLine()!);

            _bookService.FilterYearWithDetail(publicationYear);
        }
    }
}
