﻿using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Utils;

namespace MiniProject.ConsoleMenu
{
    public class LoanMenu
    {
        private readonly ILoanDetailRepository _loanDetailRepository;
        private readonly ILoanService _loanService;

        public LoanMenu(ILoanService loanService, ILoanDetailRepository loanDetailRepository)
        {
            _loanDetailRepository = loanDetailRepository;
            _loanService = loanService;
        }

        public void LoanBookListMenu()
        {
            int pageNumber = 1;
            int pageSize = 5;
            bool quit = false;
            decimal totalPageNumbers = _loanDetailRepository.GetAll().Count / pageSize;

            _loanService.LoanBookListWithDetail(pageNumber, pageSize);

            while (!quit)
            {
                Console.WriteLine(Constants.Pagination3);
                ConsoleKeyInfo choice = Console.ReadKey()!;

                if (choice.Key == ConsoleKey.N || choice.Key == ConsoleKey.P || choice.Key == ConsoleKey.B || choice.Key == ConsoleKey.Enter)
                {
                    switch (choice.Key)
                    {
                        case ConsoleKey.N:
                            if (pageNumber > Math.Ceiling(totalPageNumbers))
                            {
                                Console.Clear();
                                _loanService.LoanBookListWithDetail(pageNumber, pageSize);
                            }
                            else
                            {
                                pageNumber++;
                                Console.Clear();
                                _loanService.LoanBookListWithDetail(pageNumber, pageSize);
                            }
                            break;
                        case ConsoleKey.P:
                            if (pageNumber == 1)
                            {
                                Console.Clear();
                                _loanService.LoanBookListWithDetail(pageNumber, pageSize);
                            }
                            else
                            {
                                pageNumber--;
                                Console.Clear();
                                _loanService.LoanBookListWithDetail(pageNumber, pageSize);
                            }
                            break;
                        case ConsoleKey.Enter:
                            Console.Clear();
                            SearchLoanBookMenu();
                            break;
                        default:
                            quit = true;
                            Console.Clear();
                            break;
                    }
                }
                else
                {
                    throw new Exception(Constants.InvalidKey);
                }
            }
        }

        public void SearchLoanBookMenu()
        {
            int choice = 0;
            int choiceAmounts = 3;

            while (choice != choiceAmounts)
            {
                HardMenu.MenuLoan();
                choice = int.Parse(Console.ReadLine()!);

                switch (choice)
                {
                    case 1:
                        Console.Clear();
                        SearchAndReturnLoanMenu();
                        break;
                    case 2:
                        Console.Clear();
                        SearchLoanByTitleMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        public void SearchAndReturnLoanMenu()
        {
            Console.Write(Constants.EnterReaderName);
            string name = Console.ReadLine()!;

            _loanService.SearchLoanBookByReader(name);

            Console.Write(Constants.EnterBookId);
            int bookId = int.Parse(Console.ReadLine()!);

            Console.Write(Constants.EnterLoanId);
            int loanId = int.Parse(Console.ReadLine()!);

            _loanService.ReturnBook(name, bookId, loanId);
        }

        public void SearchLoanByTitleMenu()
        {
            Console.Write(Constants.EnterBookTitle);
            string title = Console.ReadLine()!;

            _loanService.SearchLoanReaderByTitle(title);
        }
    }
}
