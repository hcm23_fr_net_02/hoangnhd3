﻿using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Utils;

namespace MiniProject.ConsoleMenu
{
    public class BookMenu
    {
        private readonly IBookRepository _bookRepository;
        private readonly ILoanService _loanService;
        private readonly IBookService _bookService;
        private readonly ReviewMenu _reviewMenu;

        public BookMenu(IBookRepository bookRepository, ILoanService loanService, IBookService bookService, ReviewMenu reviewMenu)
        {
            _bookRepository = bookRepository;
            _loanService = loanService;
            _bookService = bookService;
            _reviewMenu = reviewMenu;
        }

        public void BookListMenu()
        {
            int pageNumber = 1;
            int pageSize = 5;
            bool quit = false;
            decimal totalPageNumbers = _bookRepository.GetAll().Count / pageSize;

            _bookService.BookListWithDetails(pageNumber, pageSize);

            while (!quit)
            {
                Console.WriteLine(Constants.Pagination1);
                ConsoleKeyInfo choice = Console.ReadKey()!;

                if (choice.Key == ConsoleKey.N || choice.Key == ConsoleKey.P || choice.Key == ConsoleKey.B || choice.Key == ConsoleKey.Enter)
                {
                    switch (choice.Key)
                    {
                        case ConsoleKey.N:
                            if (pageNumber > Math.Ceiling(totalPageNumbers))
                            {
                                Console.Clear();
                                _bookService.BookListWithDetails(pageNumber, pageSize);
                            }
                            else
                            {
                                pageNumber++;
                                Console.Clear();
                                _bookService.BookListWithDetails(pageNumber, pageSize);
                            }
                            break;
                        case ConsoleKey.P:
                            if (pageNumber == 1)
                            {
                                Console.Clear();
                                _bookService.BookListWithDetails(pageNumber, pageSize);
                            }
                            else
                            {
                                pageNumber--;
                                Console.Clear();
                                _bookService.BookListWithDetails(pageNumber, pageSize);
                            }
                            break;
                        case ConsoleKey.Enter:
                            GetBookByIdMenu();
                            break;
                        default:
                            quit = true;
                            Console.Clear();
                            break;
                    }
                }
                else
                {
                    throw new Exception(Constants.InvalidKey);
                }
            }
        }

        public void AddBookMenu()
        {
            Console.Write(Constants.EnterBookTitle);
            string title = Console.ReadLine()!;

            Console.Write(Constants.EnterBookAuthor);
            string author = Console.ReadLine()!;

            Console.Write(Constants.EnterBookGenre);
            string genre = Console.ReadLine()!;

            Console.Write(Constants.EnterBookPublicationYear);
            int publicationYear = int.Parse(Console.ReadLine()!);

            Console.Write(Constants.EnterBookQuantity);
            int quantity = int.Parse(Console.ReadLine()!);

            _bookService.AddBook(title, author, genre, publicationYear, quantity);
        }

        public void GetBookByIdMenu()
        {
            Console.Write(Constants.EnterIdForMoreInfo);
            int bookId = int.Parse(Console.ReadLine()!);

            _bookService.BookByIdWithDetails(bookId);

            BookManagementMenu(bookId);
        }

        public void BookManagementMenu(int bookId)
        {
            int choice = 0;
            int choiceAmounts = 6;

            while (choice != choiceAmounts)
            {
                HardMenu.MenuBook();
                choice = int.Parse(Console.ReadLine()!);

                if (choice < 1 || choice > choiceAmounts)
                {
                    throw new Exception(Constants.InvalidOption);
                }

                switch (choice)
                {
                    case 1:
                        Console.Clear();

                        try
                        {
                            UpdateBookMenu(bookId);
                        }
                        catch (Exception ex) 
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 2:
                        Console.Clear();

                        try
                        {
                            _bookRepository.Delete(bookId);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 3:
                        Console.Clear();

                        try
                        {
                            _reviewMenu.AddReviewMenu();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 4:
                        Console.Clear();

                        try
                        {
                            _reviewMenu.ShowReviewListMenu(bookId);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 5:
                        Console.Clear();

                        try
                        {
                            BorrowBookMenu(bookId);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }

        public void UpdateBookMenu(int bookId)
        {

            Console.Write(Constants.EnterBookTitle);
            string title = Console.ReadLine()!;

            Console.Write(Constants.EnterBookAuthor);
            string author = Console.ReadLine()!;

            Console.Write(Constants.EnterBookGenre);
            string genre = Console.ReadLine()!;

            Console.Write(Constants.EnterBookPublicationYear);
            int publicationYear = int.Parse(Console.ReadLine()!);

            Console.Write(Constants.EnterBookQuantity);
            int quantity = int.Parse(Console.ReadLine()!);

            Console.Write(Constants.EnterBookAvailability);
            bool isAvailable = bool.Parse(Console.ReadLine()!);

            _bookService.UpdateBook(bookId, title, author, genre, publicationYear, quantity, isAvailable);
        }

        public void BorrowBookMenu(int bookId)
        {
            Console.Write(Constants.EnterReaderName);
            string readerName = Console.ReadLine()!;

            Console.Write(Constants.EnterLoanQuantity);
            int loanQuantity = int.Parse(Console.ReadLine()!);

            Console.Write(Constants.EnterLoanDueDate);
            DateTime dueDate = DateTime.Parse(Console.ReadLine()!);

            _loanService.BorrowBook(bookId, readerName, loanQuantity, dueDate);
        }
    }
}
