﻿using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;

namespace MiniProject.Contracts.IRepository
{
    public interface IReaderRepository : IGenericRepository<Reader>
    {
        public Reader UpdateReader(int readerId, DTOReader dtoReader);

        public Reader GetReaderByName(string name);

        public List<Reader> GetReaderListByLoanId(int loanId);
    }
}
