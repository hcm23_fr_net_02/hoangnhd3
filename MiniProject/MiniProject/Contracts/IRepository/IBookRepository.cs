﻿using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;

namespace MiniProject.Contracts.IRepository
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        public Book UpdateBook(int bookId, DTOBook dtoBook);

        public List<Book> FilterAuthor(string author);

        public List<Book> FilterTitle(string title);

        public List<Book> FilterGenre(string genre);

        public List<Book> FilterPublicationYear(int year);

        public Book GetBookByTitle(string bookTitle);

        public void UpdateQuantityAfterReturn(Book book, LoanDetail loanDetail);
    }
}
