﻿using MiniProject.Models.DomainModels;

namespace MiniProject.Contracts.IRepository
{
    public interface ILoanDetailRepository : IGenericRepository<LoanDetail>
    {
        public LoanDetail GetLoanDetail(Loan loan, int loanId, int bookId); 

        public List<LoanDetail> LoanDetailListByBookId(int bookId);
    }
}
