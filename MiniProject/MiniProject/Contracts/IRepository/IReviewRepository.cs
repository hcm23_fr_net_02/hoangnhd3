﻿using MiniProject.Models.DomainModels;

namespace MiniProject.Contracts.IRepository
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
        public List<Review> Pagination(int pageNumber, int pageSize, int bookId);
    }
}
