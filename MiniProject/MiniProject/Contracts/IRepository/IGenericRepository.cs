﻿namespace MiniProject.Contracts.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        public T Add(T entity);

        public List<T> GetAll();

        public T GetById(int id);

        public T Delete(int id);

        public List<T> Pagination(int pageNumber, int pageSize);
    }
}
