﻿using MiniProject.Models.DomainModels;

namespace MiniProject.Contracts.IRepository
{
    public interface ILoanRepository : IGenericRepository<Loan>
    {
        public List<Loan> GetLoanListByReaderId(int readerId);

        public List<Loan> GetLoanListByLoanDetailId(int loanDetailId);

        public Loan GetLoanByLoanDetailId(int loanDetailId);
    }
}
