﻿using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;

namespace MiniProject.Contracts.IService
{
    public interface IReaderService
    {
        public List<Reader> ReaderListWithDetail(int pageNumber, int pageSize);
        public Reader AddReader(string name, string address, string phoneNumber);
        public DTOReader UpdateReader(int readerId, string name, string address, string phoneNumber);
    }
}
