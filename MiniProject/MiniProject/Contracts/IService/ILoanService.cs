﻿using MiniProject.Models.DomainModels;

namespace MiniProject.Contracts.IService
{
    public interface ILoanService
    {
        public void LoanBookListWithDetail(int pageNumber, int pageSize);
        public void SearchLoanBookByReader(string input);
        public List<Reader> SearchLoanReaderByTitle(string input);
        public void BorrowBook(int bookId, string readerName, int loanQuantity, DateTime dueDate);
        public void ReturnBook(string name, int bookId, int loanId);
    }
}
