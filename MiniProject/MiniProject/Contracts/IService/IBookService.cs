﻿using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;

namespace MiniProject.Contracts.IService
{
    public interface IBookService
    {
        public Book AddBook(string title, string author, string genre, int year, int quantity);
        public DTOBook UpdateBook(int bookId, string title, string author, string genre, int year, int quantity, bool isAvailable);
        public List<Book> BookListWithDetails(int pageNumber, int pageSize);
        public Book BookByIdWithDetails(int bookId);
        public List<Book> FilterAuthorWithDetail(string author);
        public List<Book> FilterTitleWithDetail(string title);
        public List<Book> FilterGenreWithDetail(string genre);
        public List<Book> FilterYearWithDetail(int year);
    }
}
