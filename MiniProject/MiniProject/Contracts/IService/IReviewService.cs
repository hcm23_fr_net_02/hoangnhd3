﻿using MiniProject.Models.DomainModels;

namespace MiniProject.Contracts.IService
{
    public interface IReviewService
    {
        public List<Review> ReviewListWithDetail(int pageNumber, int pageSize, int bookId);
        public Review AddReview(string title, string description, int rating);
    }
}
