﻿using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;

namespace MiniProject.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;

        public BookService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;   
        }

        public Book AddBook(string title, string author, string genre, int year, int quantity)
        {
            Book newBook = new Book();

            newBook.Title = title;
            newBook.Author = author;
            newBook.Genre = genre;
            newBook.PublicationYear = year;
            newBook.StockQuantity = quantity;
            newBook.IsAvailable = true;

            _bookRepository.Add(newBook);

            return newBook;
        }

        public DTOBook UpdateBook(int bookId, string title, string author, string genre, int year, int quantity, bool isAvailable)
        {
            DTOBook dtoBook = new DTOBook();

            dtoBook.Title = title;
            dtoBook.Author = author;
            dtoBook.Genre = genre;
            dtoBook.PublicationYear = year;
            dtoBook.Quantity = quantity;
            dtoBook.IsAvailable = isAvailable;

            _bookRepository.UpdateBook(bookId, dtoBook);

            return dtoBook;
        }

        public List<Book> BookListWithDetails(int pageNumber, int pageSize)
        {
            var books = _bookRepository.Pagination(pageNumber, pageSize);

            foreach (Book book in books)
            {
                Console.WriteLine($"Id: {book.Id}, Title: {book.Title}, Genre: {book.Genre}, Is Available: {book.IsAvailable}");
            }

            return books;
        }

        public Book BookByIdWithDetails(int bookId)
        {

            var book = _bookRepository.GetById(bookId);

            Console.WriteLine($"Id: {book.Id}, Title: {book.Title}, Author: {book.Author}, Genre: {book.Genre}, " +
                              $"Publication Year: {book.PublicationYear}, Stock Quantity: {book.StockQuantity}, Is Available: {book.IsAvailable}");
            Console.WriteLine();

            return book;
        }

        public List<Book> FilterAuthorWithDetail(string author)
        {
            var books = _bookRepository.FilterAuthor(author);

            foreach (var book in books)
            {
                Console.WriteLine($"Id: {book.Id}, Title: {book.Title}, Author: {book.Author}");
            }

            return books;
        }

        public List<Book> FilterTitleWithDetail(string title)
        {
            var books = _bookRepository.FilterTitle(title);

            foreach (var book in books)
            {
                Console.WriteLine($"Id: {book.Id}, Title: {book.Title}, Author: {book.Author}, Genre: {book.Genre}, Publication Year: {book.PublicationYear}");
            }

            return books;
        }

        public List<Book> FilterGenreWithDetail(string genre)
        {
            var books = _bookRepository.FilterGenre(genre);

            foreach (var book in books)
            {
                Console.WriteLine($"Id: {book.Id}, Title: {book.Title}, Genre: {book.Genre}");
            }

            return books;
        }

        public List<Book> FilterYearWithDetail(int year)
        {
            var books = _bookRepository.FilterPublicationYear(year);

            foreach ( var book in books)
            {
                Console.WriteLine($"Id: {book.Id}, Title: {book.Title}, Publication Year: {book.PublicationYear}");

            }

            return books;
        }
    }
}
