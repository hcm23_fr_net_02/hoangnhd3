﻿using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Models.DomainModels;

namespace MiniProject.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;

        public ReviewService(IReviewRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
        }

        public List<Review> ReviewListWithDetail(int pageNumber, int pageSize, int bookId)
        {
            var bookReviews = _reviewRepository.Pagination(pageNumber, pageSize, bookId);

            foreach (Review review in bookReviews)
            {
                Console.WriteLine($"Id: {review.Id}, BookId: {review.BookId}, Title: {review.Title}, Description: {review.Description}, Rating: {review.Rating}");
            }

            return bookReviews;
        }

        public Review AddReview(string title, string description, int rating)
        {
            Review newReview = new Review();

            newReview.Title = title;
            newReview.Description = description;
            newReview.Rating = rating;

            _reviewRepository.Add(newReview);

            return newReview;
        }
    }
}
