﻿using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;

namespace MiniProject.Services
{
    public class ReaderService : IReaderService
    {
        private readonly IReaderRepository _readerRepository;

        public ReaderService(IReaderRepository readerRepository)
        {
            _readerRepository = readerRepository;
        }

        public List<Reader> ReaderListWithDetail(int pageNumber, int pageSize)
        {
            var readers = _readerRepository.Pagination(pageNumber, pageSize);

            foreach (Reader reader in readers)
            {
                Console.WriteLine($"Id: {reader.Id}, Name: {reader.FullName}, Address: {reader.Address}, Phone Number: {reader.PhoneNumber}");
            }

            return readers;
        }

        public Reader AddReader(string name, string address, string phoneNumber)
        {
            Reader newReader = new Reader();

            newReader.FullName = name;
            newReader.Address = address;
            newReader.PhoneNumber = phoneNumber;

            _readerRepository.Add(newReader);

            return newReader;
        }

        public DTOReader UpdateReader(int readerId, string name, string address, string phoneNumber)
        {
            DTOReader dtoReader = new DTOReader();

            dtoReader.FullName = name;
            dtoReader.Address = address;
            dtoReader.PhoneNumber = phoneNumber;

            _readerRepository.UpdateReader(readerId, dtoReader);

            return dtoReader;
        }
    }
}
