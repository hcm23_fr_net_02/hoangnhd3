﻿using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Models.DomainModels;
using MiniProject.Utils;

namespace MiniProject.Services
{
    public class LoanService : ILoanService
    {
        private readonly ILoanRepository _loanRepository;
        private readonly ILoanDetailRepository _loanDetailRepository;
        private readonly IBookRepository _bookRepository;
        private readonly IReaderRepository _readerRepository;

        public LoanService(ILoanRepository loanRepository, ILoanDetailRepository loanDetailRepository, IBookRepository bookRepository, IReaderRepository readerRepository)
        {
            _loanRepository = loanRepository;
            _loanDetailRepository = loanDetailRepository;
            _bookRepository = bookRepository;
            _readerRepository = readerRepository;
        }

        public void LoanBookListWithDetail(int pageNumber, int pageSize)
        {
            var loanBookDetail = _loanRepository.GetAll().Join(_loanDetailRepository.GetAll(), l => l.Id, ld => ld.LoanId, (l, ld) => new { Loan = l, LoanDetail = ld })
                                               .Join(_bookRepository.GetAll(), lld => lld.LoanDetail.BookId, b => b.Id, (lld, b) => new { lld.Loan, lld.LoanDetail, Book = b })
                                               .Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            foreach (var loanBook in loanBookDetail)
            {
                Console.WriteLine($"Book Id: {loanBook.Book.Id}, Book Title: {loanBook.Book.Title}, " +
                                  $"Quantity: {loanBook.LoanDetail.Quantity}, Loan Date: {loanBook.Loan.LoanDate}, Due Date: {loanBook.Loan.DueDate}");
            }
        }

        public void SearchLoanBookByReader(string input)
        {
            var loanReader = _readerRepository.GetReaderByName(input);

            if (loanReader == null)
            {
                throw new Exception(Constants.ReaderNotFound);
            }

            var loanBooks = _loanRepository.GetAll().Join(_loanDetailRepository.GetAll(), l => l.Id, ld => ld.LoanId, (l, ld) => new { Loan = l, LoanDetail = ld })
                                                  .Join(_bookRepository.GetAll(), lld => lld.LoanDetail.BookId, b => b.Id, (lld, b) => new { lld.Loan, lld.LoanDetail, Book = b })
                                                  .ToList();

            var loanBookByReader = loanBooks.Where(x => x.Loan.ReaderId == loanReader!.Id).ToList();

            foreach (var book in loanBookByReader)
            {
                Console.WriteLine($"{book.Book.Title}, {book.LoanDetail.Quantity}, {book.Loan.DueDate}");
            }

        }

        public List<Reader> SearchLoanReaderByTitle(string input)
        {
            Book book = new Book();
            List<Reader> loanReaders = new List<Reader>();

            var loanBook = _bookRepository.GetBookByTitle(input);

            var bookLoansDetail = _loanDetailRepository.LoanDetailListByBookId(loanBook.Id);

            foreach (LoanDetail loanDetail in bookLoansDetail)
            {
                var loans = _loanRepository.GetLoanListByLoanDetailId(loanDetail.Id);

                foreach (Loan loan in loans)
                {
                    var readers = _readerRepository.GetReaderListByLoanId(loan.Id);

                    foreach (Reader reader in readers)
                    {
                        Console.WriteLine($"{reader.FullName}");
                    }

                    loanReaders.AddRange(readers);
                }
            }

            return loanReaders;
        }

        public void BorrowBook(int bookId, string readerName, int loanQuantity, DateTime dueDate)
        {
            var _context = new AppDbContext();

            var loanReader = _readerRepository.GetReaderByName(readerName)
                            ?? throw new Exception(Constants.ReaderNotFound);

            var loanBook = _bookRepository.GetById(bookId) ?? throw new Exception(Constants.BookNotFound);

            if (loanBook.StockQuantity < 1 || loanBook.IsAvailable == false)
            {
                throw new Exception(Constants.UnavailableForLoan);
            }

            if (loanQuantity < 0 || loanQuantity > loanBook.StockQuantity)
            {
                throw new Exception(Constants.InvalidQuantity);
            }

            loanBook.StockQuantity -= loanQuantity;
            _context.Update(loanBook);
            _context.SaveChanges();

            if (loanBook.StockQuantity == 0)
            {
                loanBook.IsAvailable = false;
                _context.SaveChanges();
            }

            Loan newLoan = new Loan();
            newLoan.ReaderId = loanReader.Id;
            newLoan.LoanDate = DateTime.Now;
            newLoan.DueDate = dueDate;

            _loanRepository.Add(newLoan);

            LoanDetail loanDetail = new LoanDetail();
            loanDetail.LoanId = newLoan.Id;
            loanDetail.BookId = loanBook.Id;
            loanDetail.Quantity = loanQuantity;

            _loanDetailRepository.Add(loanDetail);
        }

        public void ReturnBook(string readerName, int bookId, int loanId)
        {
            var _context = new AppDbContext();

            var book = _bookRepository.GetById(bookId);

            var loanReader = _readerRepository.GetReaderByName(readerName)
                             ?? throw new Exception(Constants.ReaderNotFound);

            var loans = _loanRepository.GetLoanListByReaderId(loanReader.Id);

            foreach (Loan loan in loans)
            {
                var deletedLoanDetail = _loanDetailRepository.GetLoanDetail(loan, loanId, bookId);

                if (deletedLoanDetail is not null)
                {
                    var deletedLoan = _loanRepository.GetLoanByLoanDetailId(deletedLoanDetail.Id);

                    book!.StockQuantity += deletedLoanDetail!.Quantity;

                    if (book.IsAvailable == false)
                    {
                        book.IsAvailable = true;
                    }

                    _context.Update(book);

                    _loanDetailRepository.Delete(deletedLoanDetail!.Id);

                    _loanRepository.Delete(deletedLoan!.Id);

                    _context.SaveChanges();
                }
            }
        }
    }
}
