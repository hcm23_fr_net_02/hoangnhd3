﻿namespace MiniProject.Models.DomainModels
{
    public class Loan
    {
        private int _id;
        private int _readerId;
        private DateTime _loanDate;
        private DateTime _dueDate;
        private Reader? _reader;
        private List<LoanDetail>? _loanDetails;

        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public int ReaderId
        {
            get => _readerId;
            set => _readerId = value;
        }

        public DateTime LoanDate
        {
            get => _loanDate;
            set => _loanDate = value;
        }

        public DateTime DueDate
        {
            get => _dueDate;
            set => _dueDate = value;
        }

        public Reader? Reader
        {
            get => _reader;
            set => _reader = value;
        }

        public List<LoanDetail> LoanDetails
        {
            get => _loanDetails!;
            set => _loanDetails = value;
        }
    }
}
