﻿using MiniProject.Utils;

namespace MiniProject.Models.DomainModels
{
    public class Book
    {
        private int _id;
        private string? _title;
        private string? _author;
        private string? _genre;
        private int _publicationYear;
        private int _stockQuantity;
        private bool _isAvailable;
        private List<Review>? _reviews;
        private List<LoanDetail>? _loanDetails;

        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string? Title
        {
            get => _title;
            set => _title = value;
        }

        public string? Author
        {
            get => _author;
            set => _author = value;
        }

        public string? Genre
        {
            get => _genre;
            set => _genre = value;
        }

        public int PublicationYear
        {
            get => _publicationYear;
            set => _publicationYear = value;
        }

        public int StockQuantity
        {
            get => _stockQuantity;
            set => _stockQuantity = value;

        }

        public bool IsAvailable
        {
            get => _isAvailable;
            set => _isAvailable = value;
        }

        public List<Review> Reviews
        {
            get => _reviews!;
            set => _reviews = value;
        }

        public List<LoanDetail> LoanDetails
        {
            get => _loanDetails!;
            set => _loanDetails = value;
        }
    }
}
