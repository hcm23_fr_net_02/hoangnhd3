﻿namespace MiniProject.Models.DomainModels
{
    public class Review
    {
        private int _id;
        private int _bookId;
        private string? _title;
        private string? _description;
        private int _rating;
        private Book? _book;

        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public int BookId
        {
            get => _bookId;
            set => _bookId = value;
        }

        public string? Title
        {
            get => _title;
            set => _title = value;
        }

        public string? Description
        {
            get => _description;
            set => _description = value;
        }

        public int Rating
        {
            get => _rating;
            set => _rating = value;
        }

        public Book Book
        {
            get => _book!;
            set => _book = value;
        }
    }
}
