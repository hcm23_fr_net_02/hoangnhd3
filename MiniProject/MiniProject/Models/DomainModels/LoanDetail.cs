﻿namespace MiniProject.Models.DomainModels
{
    public class LoanDetail
    {
        private int _id;
        private int _quantity;
        private int _loanId;
        private Loan? _loan;
        private int _bookId;
        private Book? _book;

        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public int Quantity
        {
            get => _quantity;
            set => _quantity = value;
        }

        public int LoanId
        {
            get => _loanId;
            set => _loanId = value;
        }

        public Loan Loan
        {
            get => _loan!;
            set => _loan = value;
        }

        public int BookId
        {
            get => _bookId;
            set => _bookId = value;
        }

        public Book Book
        {
            get => _book!;
            set => _book = value;
        }
    }
}
