﻿namespace MiniProject.Models.DomainModels
{
    public class Reader
    {
        private int _id;
        private string? _fullName;
        private string? _address;
        private string? _phoneNumber;
        private List<Loan>? _loans;

        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string? FullName
        {
            get => _fullName;
            set => _fullName = value;
        }

        public string? Address
        {
            get => _address;
            set => _address = value;
        }

        public string PhoneNumber
        {
            get => _phoneNumber!;
            set => _phoneNumber = value;
        }

        public List<Loan> Loans
        {
            get => _loans!;
            set => _loans = value;
        }
    }
}
