﻿namespace MiniProject.Models.DTOModels
{
    public class DTOReader
    {
        private string? _fullName;
        private string? _address;
        private string? _phoneNumber;

        public string FullName
        {
            get => _fullName!;
            set => _fullName = value;
        }

        public string Address
        {
            get => _address!;
            set => _address = value;
        }

        public string PhoneNumber
        {
            get => _phoneNumber!;
            set => _phoneNumber = value;
        }
    }
}
