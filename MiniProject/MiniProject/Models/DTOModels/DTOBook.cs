﻿using MiniProject.Utils;

namespace MiniProject.Models.DTOModels
{
    public class DTOBook
    {
        public string? Title { get; set; }
        public string? Author { get; set; }
        public string? Genre { get; set; }
        public int PublicationYear { get; set; }
        public int Quantity { get; set; }
        public bool IsAvailable { get; set; }
    }
}
