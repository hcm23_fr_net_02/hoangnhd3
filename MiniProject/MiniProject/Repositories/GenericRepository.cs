﻿using MiniProject.Contracts.IRepository;
using MiniProject.Utils;

namespace MiniProject.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        public readonly AppDbContext _context;

        public GenericRepository(AppDbContext context)
        {
            _context = context;
        }

        public T Add(T entity)
        {
            var dbSet = _context.Set<T>();

            dbSet.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public List<T> GetAll()
        {
            var dbSet = _context.Set<T>();

            return dbSet.ToList();
        }

        public T GetById(int id)
        {
            var dbSet = _context.Set<T>();

            return dbSet.AsEnumerable().FirstOrDefault(x => (int)x.GetType().GetProperty("Id")!.GetValue(x)! == id)!;
        }

        public T Delete(int id)
        {
            var dbSet = _context.Set<T>();

            var deletedItem = GetById(id);
            dbSet.Remove(deletedItem);
            _context.SaveChanges();

            return deletedItem;
        }

        public List<T> Pagination(int pageNumber, int pageSize)
        {
            var dbSet = _context.Set<T>();

            return dbSet.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
