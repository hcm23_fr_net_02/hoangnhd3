﻿using MiniProject.Contracts.IRepository;
using MiniProject.Models.DomainModels;
using MiniProject.Utils;

namespace MiniProject.Repositories
{
    public class LoanDetailRepository : GenericRepository<LoanDetail>, ILoanDetailRepository
    {
        public LoanDetailRepository(AppDbContext context) : base(context)
        {
        }

        public LoanDetail GetLoanDetail(Loan loan, int loanId, int bookId)
        {
            var loanDetail = GetAll().FirstOrDefault(ld => loan.Id == loanId && ld.BookId == bookId);

            return loanDetail!;
        }

        public List<LoanDetail> LoanDetailListByBookId(int bookId)
        {
            var loanDetails = GetAll().Where(detail => detail.BookId == bookId).ToList();

            return loanDetails;
        }
    }
}
