﻿using MiniProject.Contracts.IRepository;
using MiniProject.Models.DomainModels;
using MiniProject.Utils;

namespace MiniProject.Repositories
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository(AppDbContext context) : base(context)
        {
        }

        public List<Review> Pagination(int pageNumber, int pageSize, int bookId)
        {
            var dbSet = _context.Set<Review>();

            return dbSet.Where(review => review.BookId == bookId).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
