﻿using MiniProject.Contracts.IRepository;
using MiniProject.Models.DomainModels;
using MiniProject.Utils;

namespace MiniProject.Repositories
{
    public class LoanRepository : GenericRepository<Loan>, ILoanRepository
    {
        public LoanRepository(AppDbContext context) : base(context)
        {
        }

        public List<Loan> GetLoanListByReaderId(int readerId)
        {
            var loanList = GetAll().Where(loan => loan.ReaderId == readerId).ToList();

            return loanList;
        }

        public List<Loan> GetLoanListByLoanDetailId(int loanDetailId)
        {
            var loans = GetAll().Where(loan => loan.Id == loanDetailId).ToList();

            return loans;
        }

        public Loan GetLoanByLoanDetailId(int loanDetailId)
        {
            var loan = GetAll().FirstOrDefault(x => x.Id == loanDetailId);

            return loan!;
        }
    }
}
