﻿using MiniProject.Contracts.IRepository;
using MiniProject.Migrations;
using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;
using MiniProject.Utils;

namespace MiniProject.Repositories
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public BookRepository(AppDbContext context) : base(context)
        {
        }

        public Book UpdateBook(int bookId, DTOBook dtoBook)
        {
            var updatedBook = GetById(bookId);

            updatedBook.Title = dtoBook.Title;
            updatedBook.Author = dtoBook.Author;
            updatedBook.Genre = dtoBook.Genre;
            updatedBook.StockQuantity = dtoBook.Quantity;
            updatedBook.PublicationYear = dtoBook.PublicationYear;
            updatedBook.IsAvailable = dtoBook.IsAvailable;

            _context.Update(updatedBook);
            _context.SaveChanges();

            return updatedBook;
        }

        public List<Book> FilterAuthor(string author)
        {
            var filteredBooks = GetAll().Where(book => book.Author!.Contains(author)).ToList();

            return filteredBooks;
        }

        public List<Book> FilterTitle(string title)
        {
            var filteredBooks = GetAll().Where(book => book.Title!.Contains(title)).ToList();

            return filteredBooks;
        }

        public List<Book> FilterGenre(string genre)
        {
            var selectedGenre = Enum.GetValues(typeof(Genre)).Cast<Genre>()
                                    .FirstOrDefault(x => x.ToString()
                                    .Equals(genre, StringComparison.OrdinalIgnoreCase)).ToString();

            var filteredBooks = GetAll().Where(book => book.Genre!.Contains(selectedGenre)).ToList();

            return filteredBooks;
        }

        public List<Book> FilterPublicationYear(int year)
        {
            var filteredBooks = GetAll().Where(book => book.PublicationYear == year).ToList();

            return filteredBooks;
        }

        public Book GetBookByTitle(string bookTitle)
        {
            var book = GetAll().FirstOrDefault(book => book.Title == bookTitle);

            return book!;
        }
    }
}
