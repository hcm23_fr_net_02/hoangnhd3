﻿using MiniProject.Contracts.IRepository;
using MiniProject.Models.DomainModels;
using MiniProject.Models.DTOModels;
using MiniProject.Utils;

namespace MiniProject.Repositories
{
    public class ReaderRepository : GenericRepository<Reader>, IReaderRepository
    {
        public ReaderRepository(AppDbContext context) : base(context)
        {
        }

        public Reader UpdateReader(int readerId, DTOReader dtoReader)
        {
            var updatedReader = GetById(readerId);

            updatedReader.FullName = dtoReader.FullName;
            updatedReader.Address = dtoReader.Address;
            updatedReader.PhoneNumber = dtoReader.PhoneNumber;

            _context.Update(updatedReader);
            _context.SaveChanges();

            return updatedReader;
        }

        public Reader GetReaderByName(string readerName)
        {
            var loanReader = GetAll().FirstOrDefault(reader => reader.FullName == readerName);

            return loanReader!;
        }

        public List<Reader> GetReaderListByLoanId(int loanId)
        {
            var readers = GetAll().Where(x => x.Id == loanId).ToList();

            return readers;
        }
    }
}
