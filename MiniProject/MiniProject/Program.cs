﻿using MiniProject.ConsoleMenu;
using MiniProject.Contracts.IRepository;
using MiniProject.Contracts.IService;
using MiniProject.Repositories;
using MiniProject.Services;
using MiniProject.Utils;

AppDbContext context = new AppDbContext();

IBookRepository bookRepository = new BookRepository(context);
ILoanRepository loanRepository = new LoanRepository(context);
ILoanDetailRepository loanDetailRepository = new LoanDetailRepository(context);
IReviewRepository reviewRepository = new ReviewRepository(context);
IReaderRepository readerRepository = new ReaderRepository(context);

IReviewService reviewService = new ReviewService(reviewRepository);
ILoanService loanService = new LoanService(loanRepository, loanDetailRepository, bookRepository, readerRepository);
IReaderService readerService = new ReaderService(readerRepository);
IBookService bookService = new BookService(bookRepository);

ReviewMenu reviewMenu = new ReviewMenu(reviewRepository, reviewService);
BookMenu bookMenu = new BookMenu(bookRepository, loanService, bookService, reviewMenu);
ReaderMenu readerMenu = new ReaderMenu(readerRepository, readerService);
FilterMenu filterMenu = new FilterMenu(bookService);
LoanMenu loanMenu = new LoanMenu(loanService, loanDetailRepository);
MainMenu mainMenu = new MainMenu(bookMenu, readerMenu, filterMenu, loanMenu);

try 
{
    mainMenu.MainMenuManager();
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}