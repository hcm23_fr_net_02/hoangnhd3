﻿namespace MiniProject.Utils
{
    public enum Genre
    {
        Action,
        Adventure,
        Children,
        Comedy,
        Crime,
        Documentary,
        Drama,
        Fantasy,
        Horror,
        Romance,
        Thriller,
        War,
        Western
    }
}
