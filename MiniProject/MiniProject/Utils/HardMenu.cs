﻿namespace MiniProject.Utils
{
    public static class HardMenu
    {
        public static void MenuMain()
        {
            Console.WriteLine($"{1}. Show book list in library");
            Console.WriteLine($"{2}. Add book to library");
            Console.WriteLine($"{3}. Filter book");
            Console.WriteLine($"{4}. Show loan list in library");
            Console.WriteLine($"{5}. Manage readers in library");
            Console.WriteLine($"{6}. Exit");
            Console.Write($"Please choose an option: ");
        }

        public static void MenuBook()
        {
            Console.WriteLine($"{1}. Update book information");
            Console.WriteLine($"{2}. Delete book");
            Console.WriteLine($"{3}. Add review");
            Console.WriteLine($"{4}. Show review list");
            Console.WriteLine($"{5}. Borrow book");
            Console.WriteLine($"{6}. Back");
            Console.Write("Please choose an option: ");
        }

        public static void MenuReader()
        {
            Console.WriteLine($"{1}. Add reader");
            Console.WriteLine($"{2}. Show reader list");
            Console.WriteLine($"{3}. Update reader info");
            Console.WriteLine($"{4}. Delete reader");
            Console.WriteLine($"{5}. Back");
            Console.Write("Please choose an option: ");
        }

        public static void MenuLoan()
        {
            Console.WriteLine($"{1}. Search loan by name");
            Console.WriteLine($"{2}. Search loan by book title");
            Console.WriteLine($"{3}. Back");
            Console.Write("Please choose an option: ");
        }

        public static void MenuFilter()
        {
            Console.WriteLine($"{1}. Filter by title");
            Console.WriteLine($"{2}. Filter by author");
            Console.WriteLine($"{3}. Filter by genre");
            Console.WriteLine($"{4}. Filter by publication year");
            Console.WriteLine($"{5}. Back to main menu");
            Console.Write("Please choose an option: ");
        }
    }
}
