﻿namespace MiniProject.Utils
{
    public static class Constants
    {
        // PAGINATION
        public const string Pagination1 = $"N: Next Page - P: Previous Page - B: Back to Main Menu";
        public const string Pagination2 = "N: Next Page - P: Previous Page - B: Back to Main Menu - Enter: Get book detail";
        public const string Pagination3 = "N: Next Page - P: Previous Page - B: Back to Main Menu - Enter: More loan features";

        // BOOK
        public const string EnterBookId = "Please enter book id: ";
        public const string EnterBookTitle = "Please enter book title: ";
        public const string EnterBookAuthor = "Please enter book author: ";
        public const string EnterBookGenre = "Please enter book genre: ";
        public const string EnterBookPublicationYear = "Please enter book publication year: ";
        public const string EnterBookQuantity = "Please enter book quantity: ";
        public const string EnterBookAvailability = "Please enter availability: ";
        public const string EnterIdForMoreInfo = "Please enter book id for more info: ";

        // REVIEW
        public const string EnterReviewTitle = "Please enter review title: ";
        public const string EnterReviewDesc = "Please enter description: ";
        public const string EnterReviewRating = "Please enter rating: ";

        // READER
        public const string EnterReaderId = "Please enter reader id: ";
        public const string EnterReaderName = "Please enter name: ";
        public const string EnterReaderAddress = "Please enter address: ";
        public const string EnterReaderNumber = "Please enter phone number: ";

        // LOAN
        public const string EnterLoanId = "Please enter loan id: ";
        public const string EnterLoanQuantity = "Please enter loan quantity: ";
        public const string EnterLoanDueDate = "Please enter due date: ";

        //EXCEOPTION
        public const string InvalidOption = "Invalid Option!";
        public const string InvalidKey = "Invalid Key!";
        public const string InvalidQuantity = "Invalid Quantity!";
        public const string BookNotFound = "Book Not Found!";
        public const string ReaderNotFound = "Reader Not Found!";
        public const string UnavailableForLoan = "Book Is Unavailable For Loan!";
    }
}
