﻿using Microsoft.EntityFrameworkCore;
using MiniProject.Models.DomainModels;

namespace MiniProject.Utils
{
    public class AppDbContext : DbContext
    {
        public DbSet<Book> books { get; set; }
        public DbSet<Reader> reader { get; set; }
        public DbSet<Review> reviews { get; set; }
        public DbSet<Loan> loans { get; set; }
        public DbSet<LoanDetail> loansDetail { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connect = "Data Source=.;Database=MiniProject;Trusted_Connection=True;TrustServerCertificate=True";
            optionsBuilder.UseSqlServer(connect);
        }
    }
}
